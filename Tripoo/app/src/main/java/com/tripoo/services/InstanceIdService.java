package com.tripoo.services;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.sendbird.android.SendBird;
import com.sendbird.android.SendBirdException;
import com.tripoo.R;
import com.tripoo.rest.RestRetriever;
import com.tripoo.rest.RestService;
import com.tripoo.rest.model.post.response.Token;
import com.tripoo.utils.Constants;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by emil on 1/21/17.
 */

public class InstanceIdService extends FirebaseInstanceIdService {

    private static final String TAG = InstanceIdService.class.getSimpleName();

    @Override
    public void onTokenRefresh() {
        SendBird.init(getString(R.string.send_bird_app_id), this);
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.i(TAG, "Firebase Token: " + refreshedToken);

        sendRegistrationTokenToSendBirdServer(refreshedToken);
        sendRegistrationTokenToREST(refreshedToken);
    }

    public void sendRegistrationTokenToREST(final String refreshedToken) {
        SharedPreferences sharedPreferences = getSharedPreferences(Constants.userDetailsSharedPreferences, Context.MODE_PRIVATE);
        String token = sharedPreferences.getString(Constants.token, "");
        long userId = sharedPreferences.getLong(Constants.id, 0);

        RestService service = RestRetriever.getClient().create(RestService.class);
        Token body = new Token();
        body.setToken(refreshedToken);

        Call<String> call = service.patchToken(token, userId, body);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                Log.i(TAG, "Refreshed token!");
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.e(TAG, "Not refreshed token!");
            }
        });
    }

    private void sendRegistrationTokenToSendBirdServer(String token) {
        SendBird.registerPushTokenForCurrentUser(token, new SendBird.RegisterPushTokenWithStatusHandler() {
            @Override
            public void onRegistered(SendBird.PushTokenRegistrationStatus pushTokenRegistrationStatus, SendBirdException e) {
                if (e != null) {
                    e.getCode();
                    Log.d(TAG, e.getMessage());
                    return;
                }

                if (pushTokenRegistrationStatus == SendBird.PushTokenRegistrationStatus.PENDING) {
                    //TODO: register token after successful connection
                }
            }
        });
    }


}
