package com.tripoo.fragment.main;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.tripoo.R;
import com.tripoo.custom.CustomScrollView;
import com.tripoo.rest.RestRetriever;
import com.tripoo.rest.RestService;
import com.tripoo.rest.adapter.RouteAdapter;
import com.tripoo.rest.model.get.details.RouteDetails;
import com.tripoo.rest.model.get.details.TripDetails;
import com.tripoo.utils.Constants;

import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by emil on 1/8/17.
 */

public class TripFragment extends Fragment {

    private static final String TAG = TripFragment.class.getSimpleName();

    private Bundle savedInstanceState;

    @BindView(R.id.routeMap)
    public MapView mapView;

    @BindView(R.id.customScrollView)
    public CustomScrollView customScrollView;

    @BindView(R.id.tripRecyclerView)
    public RecyclerView recyclerView;

    @BindView(R.id.luggageImgView)
    public ImageView luggageImageView;

    @BindView(R.id.luggageSizeTxtView)
    public TextView luggageTextView;

    @BindView(R.id.tripPriceTxtView)
    public TextView tripPriceTextView;

    @BindView(R.id.tripCommentTxtView)
    public TextView tripCommentTextView;

    @BindView(R.id.tripDateTextView)
    public TextView tripDateTextView;


    public GoogleMap googleMap;
    private TripDetails tripDetails;
    private Long tripId;

    public TripFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        this.savedInstanceState = savedInstanceState;
        tripId = getArguments().getLong("trip_id");

        makeRestCall(tripId);
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.trip_scroll_layout, container, false);
        ButterKnife.bind(this, view);
        return view;
    }


    private void makeRestCall(final Long tripId) {
        RestService service = RestRetriever.getClient().create(RestService.class);
        Call<TripDetails> call = service.getTripById(tripId, "trip_profile");
        Log.i(TAG, "TripId: " + tripId);


        call.enqueue(new Callback<TripDetails>() {
            @Override
            public void onResponse(Call<TripDetails> call, Response<TripDetails> response) {
                tripDetails = response.body();
                Log.i(TAG, "set up routes");


                setUpMap(tripDetails.getRoutes());
            }

            @Override
            public void onFailure(Call<TripDetails> call, Throwable t) {
                Toast.makeText(getActivity(), "TripFragment! TripId: " + tripId, Toast.LENGTH_SHORT).show();//R.string.connection_failure
                Log.d(TAG, t.getMessage());
                t.printStackTrace();
                tripDetails = null;
            }
        });
    }


    private void setUpMap(final List<RouteDetails> routes) {
        setRouteRecyclerView(routes);
        setLuggageView(tripDetails.getLuggage());
        setTripPrice(tripDetails.getPrice());
        setTripCommentTextView(tripDetails.getTripComment());
//        setCarTextView();
        setDateTextView(tripDetails.getDate().toString());

        mapView.onCreate(savedInstanceState);
        mapView.onResume(); // display map immediately

        MapsInitializer.initialize(getActivity().getApplicationContext());
        
        mapView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                customScrollView.requestDisallowInterceptTouchEvent(true);
                return mapView.onTouchEvent(motionEvent);
            }


        });

        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap map) {
                googleMap = map;

                if (routes != null && routes.size() != 0) {
                    for (RouteDetails route : routes) {
                        if (route != null) {

                            LatLng latLng = new LatLng(route.getStartLatitude(), route.getStartLongitude());
                            addMarkerToMap(latLng,
                                    route.getStartPoint(), route.getStartTime());

                            if (route.equals(routes.get(routes.size() - 1))) {
                                Log.d(TAG, route.getEndLatitude() + ":" + route.getEndLongitude() + "");
                                addMarkerToMap(new LatLng(route.getEndLatitude(), route.getEndLongitude()),

                                        route.getEndPoint(), route.getStartTime());
                            }
                            CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(42.423471, 25.028068)).zoom(6).build();
                            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                        }
                    }
                }
            }
        });
    }


    private void setDateTextView(String date) {
        tripDateTextView.setText(date);
    }


    private void setCarTextView() {
        //TODO: add car
    }

    private void setTripPrice(Integer price) {
        tripPriceTextView.setText(Integer.toString(price));
    }

    private void setTripCommentTextView(String comment) {
        tripCommentTextView.setText(comment);
    }

    private void setLuggageView(String luggage) {
        switch (luggage.toLowerCase()) {
            case "small":
                luggageImageView.setImageResource(R.drawable.ic_bag_small);
                break;
            case "medium":
                luggageImageView.setImageResource(R.drawable.ic_bag_pic);
                break;
            case "large":
                luggageImageView.setImageResource(R.drawable.ic_bag_large);
                break;
        }
        luggageTextView.setText(luggage);
    }

    private void addMarkerToMap(LatLng latLng, String title, String description) {
        googleMap.addMarker(new MarkerOptions().position(latLng).title(title).snippet(description));
    }

    private void setRouteRecyclerView(List<RouteDetails> routes) {
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        checkIfItIsLoggedUserTrip();
        recyclerView.setAdapter(new RouteAdapter(routes, R.layout.list_routes, getActivity(), checkIfItIsLoggedUserTrip()));
    }

    private boolean checkIfItIsLoggedUserTrip() {
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(Constants.userDetailsSharedPreferences, Context.MODE_PRIVATE);
        Set<String> stringSet = sharedPreferences.getStringSet(Constants.user_trips, null);

        if(stringSet != null){
            for(String str: stringSet){
                if(Long.parseLong(str) == tripId)
                    return true;
            }
        }
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
//        mapView.onResume();
    }


    @Override
    public void onPause() {
        super.onPause();
//        mapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
//        mapView.onLowMemory();
    }
}
