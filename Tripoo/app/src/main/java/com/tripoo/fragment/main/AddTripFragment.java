package com.tripoo.fragment.main;

import android.app.DatePickerDialog;
import android.app.Fragment;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import com.tripoo.R;
import com.tripoo.rest.model.post.request.Trip;
import com.tripoo.utils.Constants;
import com.tripoo.utils.NumberToMonthConverter;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by emil on 1/6/17.
 */
public class AddTripFragment extends Fragment {

    private static final String TAG = AddTripFragment.class.getSimpleName();
    @BindView(R.id.pickUpTimeBtn)
    public Button pickUpTimeBtn;

    @BindView(R.id.luggageSpinner)
    public Spinner luggageSpinner;

    @BindView(R.id.pickUpDateBtn)
    public Button pickUpDateBtn;

    @BindView(R.id.tripCommentEdtTxt)
    public EditText tripCommentEdtTxt;

    @BindView(R.id.freePlacesEdtTxt)
    public EditText freePlacesEdtTxt;

    @BindView(R.id.createTrip)
    public Button createTripBtn;

    private Calendar myCalendar = Calendar.getInstance();
    private String token;
    private Trip trip;

    private int dateStampMinute;
    private int dateStampHour;
    private int dateStampMonth;
    private int dateStampYear;
    private int dateStampDay;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        token = getArguments().getString(Constants.token);
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.add_layout, container, false);
        ButterKnife.bind(this, view);
        trip = new Trip();

        pickUpDateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(getActivity(), date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        createTripBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int freePlaces = Integer.parseInt(freePlacesEdtTxt.getText().toString());
                trip.setTripComment(tripCommentEdtTxt.getText().toString());

                final android.app.FragmentManager manager = getFragmentManager();
                AddRouteFragment fragment = new AddRouteFragment();
                Date timeStamp = new Date(dateStampYear, dateStampMonth, dateStampDay, dateStampHour, dateStampMinute);
//                Toast.makeText(getActivity(), "TimeStamp: " + timeStamp.getTime(), Toast.LENGTH_SHORT).show();

                Bundle arguments = new Bundle();
                arguments.putInt("places", freePlaces);
                arguments.putString("token", token);
                arguments.putLong("date", timeStamp.getTime());
                arguments.putSerializable("trip", trip);
                fragment.setArguments(arguments);

                manager.beginTransaction().replace(R.id.addActivityFrameLayout, fragment).commit();
            }
        });

        pickUpTimeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar currentTime = Calendar.getInstance();
                int hour = currentTime.get(Calendar.HOUR_OF_DAY);
                int minute = currentTime.get(Calendar.MINUTE);
                TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int pickedHour, int pickedMinute) {
                        dateStampHour = pickedHour;
                        dateStampMinute = pickedMinute;
                        pickUpTimeBtn.setText(convertMinutesAndHours(pickedHour) + ":" + convertMinutesAndHours(pickedMinute));

                    }
                }, hour, minute, true);
                timePickerDialog.show();
            }
        });

        initializeSpinner();

        return view;
    }


    private String convertMinutesAndHours(int i) {
        if (i < 10)
            return "0" + i;
        else
            return i + "";
    }

    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker datePicker, int year, int monthOfYear,
                              int dayOfMonth) {
            dateStampYear = datePicker.getYear();
            dateStampMonth = datePicker.getMonth();
            dateStampDay = datePicker.getDayOfMonth();

            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            trip.setDate(myCalendar.getTime().getTime());
            updateLabel();
        }

    };

    private void updateLabel() {
        pickUpDateBtn.setText(NumberToMonthConverter.convertDate(NumberToMonthConverter.formatDate(myCalendar.getTime())));
    }




    private void initializeSpinner() {
        final ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(), R.array.luggage_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        luggageSpinner.setAdapter(adapter);

        luggageSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(getActivity(), "Selected: " + adapterView.getSelectedItem().toString(), Toast.LENGTH_SHORT).show();
                trip.setLuggage(adapterView.getSelectedItem().toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Toast.makeText(getActivity(), "Nothing selected: " + adapterView.getSelectedItem().toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

}
