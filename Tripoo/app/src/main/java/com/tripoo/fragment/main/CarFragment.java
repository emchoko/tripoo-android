package com.tripoo.fragment.main;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.tripoo.R;
import com.tripoo.rest.RestRetriever;
import com.tripoo.rest.RestService;
import com.tripoo.rest.adapter.CarAdapter;
import com.tripoo.rest.model.get.details.CarDetails;
import com.tripoo.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by emil on 1/17/17.
 */

public class CarFragment extends Fragment {

    private static final String TAG = CarFragment.class.getSimpleName();
    @BindView(R.id.carsRecylcerView)
    public RecyclerView recyclerView;

    private List<CarDetails> listCars;
    private Long userId;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        listCars = new ArrayList<>();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.cars_layout, container, false);
        ButterKnife.bind(this, view);

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(Constants.userDetailsSharedPreferences, Context.MODE_PRIVATE);
        userId = sharedPreferences.getLong(Constants.id, 0);

        retrieveUserCars(userId);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(Constants.userDetailsSharedPreferences, Context.MODE_PRIVATE);
        userId = sharedPreferences.getLong(Constants.id, 0);
        retrieveUserCars(userId);
    }

    private void retrieveUserCars(Long userId) {

        RestService service = RestRetriever.getClient().create(RestService.class);
        Call<List<CarDetails>> call = service.getCarsByUserId(userId, "car_profile");

        call.enqueue(new Callback<List<CarDetails>>() {
            @Override
            public void onResponse(Call<List<CarDetails>> call, Response<List<CarDetails>> response) {
                listCars = response.body();
                if (listCars != null && listCars.size() > 0)
                    setRecyclerView(R.layout.car_info_layout, listCars);
            }

            @Override
            public void onFailure(Call<List<CarDetails>> call, Throwable t) {
                Toast.makeText(getActivity(), R.string.connection_failure, Toast.LENGTH_SHORT).show();
                Log.d(TAG, "Localized Message: " + t.getLocalizedMessage()
                        + "\nMessage" + t.getMessage()
                        + "\nCause: " + t.getCause()
                        + "\nStackTrace: " + t.getStackTrace().toString());
            }
        });
    }

    private void setRecyclerView(int rowLayout, List<CarDetails> cars) {
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(new CarAdapter(cars, rowLayout, getActivity()));
    }
}
