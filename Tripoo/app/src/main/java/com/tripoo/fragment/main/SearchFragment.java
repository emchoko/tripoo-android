package com.tripoo.fragment.main;

import android.app.DatePickerDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.tripoo.R;
import com.tripoo.rest.RestRetriever;
import com.tripoo.rest.RestService;
import com.tripoo.rest.adapter.TripAdapter;
import com.tripoo.rest.model.get.list.TripList;
import com.tripoo.rest.model.post.request.Alert;
import com.tripoo.utils.Constants;
import com.tripoo.utils.LocationHelper;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static com.google.android.gms.location.places.AutocompleteFilter.TYPE_FILTER_CITIES;

/**
 * Created by emil on 1/6/17.
 */

public class SearchFragment extends Fragment {


    private static final String TAG = SearchFragment.class.getSimpleName();
    private static final int PLACE_AUTOCOMPLETE_REQUEST_CODE_START_POINT = 341;
    private static final int PLACE_AUTOCOMPLETE_REQUEST_CODE_END_POINT = 342;

    private static final String START_BUTTON_TXT = "CHOOSE START POINT";
    private static final String END_BUTTON_TXT = "CHOOSE END POINT";
    private static final String DATE_BUTTON_TXT = "CHOOSE DATE";


    @BindView(R.id.searchGetAllTrips)
    public Button searchButton;

    @BindView(R.id.pickDateBtn)
    public Button pickDateButton;

    @BindView(R.id.tripSearchRecyclerView)
    public RecyclerView recyclerView;

    @BindView(R.id.buttonStartPoint)
    public Button startPointButton;

    @BindView(R.id.buttonEndPoint)
    public Button endPointButton;

    @BindView(R.id.createAlertBtn)
    public Button createButton;

    private MaterialDialog materialDialog;

    private String startPoint = "";
    private String endPoint = "";
    private Calendar myCalendar = Calendar.getInstance();


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE_START_POINT) {
            if (resultCode == RESULT_OK) {

                Place place = PlaceAutocomplete.getPlace(getActivity(), data);
                startPoint = LocationHelper.convertLatLngToEnglishString(place.getLatLng(), getActivity());
                if (startPoint != null)
                    startPointButton.setText(startPoint);
                else
                    LocationHelper.makeToastForProblemWithGeocoder(getActivity());

                checkIfFieldsAreFull();
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(getActivity(), data);
                Toast.makeText(getActivity(), status.getStatusMessage(), Toast.LENGTH_SHORT).show();
            }
        } else if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE_END_POINT) {
            if (resultCode == RESULT_OK) {

                Place place = PlaceAutocomplete.getPlace(getActivity(), data);
                endPoint = LocationHelper.convertLatLngToEnglishString(place.getLatLng(), getActivity());
                if (endPoint != null)
                    endPointButton.setText(endPoint);
                else
                    LocationHelper.makeToastForProblemWithGeocoder(getActivity());

                checkIfFieldsAreFull();
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(getActivity(), data);
                Toast.makeText(getActivity(), status.getStatusMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.search_layout, container, false);
        ButterKnife.bind(this, view);

        Log.d(TAG, "Internet Avaivable: " + isNetworkAvailable());

        initViews();

        return view;
    }

    private void initViews() {
        startPointButton.setText(START_BUTTON_TXT);
        endPointButton.setText(END_BUTTON_TXT);
        pickDateButton.setText(DATE_BUTTON_TXT);
        searchButton.setEnabled(false);
        createButton.setEnabled(false);
    }

    @OnClick(R.id.createAlertBtn)
    public void createAlertClick() {
        postAlertToRest(startPoint, endPoint, myCalendar.getTime());
    }

    private void postAlertToRest(String startPoint, String endPoint, Date time) {
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(Constants.userDetailsSharedPreferences, Context.MODE_PRIVATE);
        String token = sharedPreferences.getString(Constants.token, "");
        
        Alert alert = new Alert(startPoint, endPoint, time.getTime());
        
        RestService service = RestRetriever.getClient().create(RestService.class);
        Call<Void> call = service.createAlert(token, alert);
        
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Toast.makeText(getActivity(), "Alert added!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Toast.makeText(getActivity(), "Alert creation failed!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @OnClick(R.id.searchGetAllTrips)
    public void searchClick() {
        materialDialog = new MaterialDialog.Builder(getActivity())
                .title("Searching for trips ...")
                .content("Please wait")
                .progress(true, 0)
                .show();
        makeRestCall(startPoint, endPoint, myCalendar.getTime());
    }


    @OnClick(R.id.buttonEndPoint)
    public void buttonEndClick() {
        try {
            AutocompleteFilter filterCities = new AutocompleteFilter.Builder()
                    .setTypeFilter(TYPE_FILTER_CITIES)
                    .build();

            Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                    .setFilter(filterCities)
                    .build(getActivity());
            startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE_END_POINT);
        } catch (GooglePlayServicesRepairableException e) {
            Toast.makeText(getActivity(), R.string.google_play_services_repairable, Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            Toast.makeText(getActivity(), R.string.google_play_services_not_available, Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    @OnClick(R.id.pickDateBtn)
    public void pickDateClick() {
        new DatePickerDialog(getActivity(), date, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    @OnClick(R.id.buttonStartPoint)
    public void buttonStartClick() {
        try {
            AutocompleteFilter filterCities = new AutocompleteFilter.Builder()
                    .setTypeFilter(TYPE_FILTER_CITIES)
                    .build();


            Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                    .setFilter(filterCities)
                    .setBoundsBias(new LatLngBounds(
                            new LatLng(-13.271484375, 36.5626000374),
                            new LatLng(50.9765625, 72.262310024)))
                    .build(getActivity());
            startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE_START_POINT);
        } catch (GooglePlayServicesRepairableException e) {
            Toast.makeText(getActivity(), R.string.google_play_services_repairable, Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            Toast.makeText(getActivity(), R.string.google_play_services_not_available, Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    private void checkIfFieldsAreFull() {
        if (!pickDateButton.getText().equals(DATE_BUTTON_TXT) && !startPointButton.getText().equals(START_BUTTON_TXT) && !endPointButton.getText().equals(END_BUTTON_TXT)) {
            searchButton.setEnabled(true);
            createButton.setEnabled(true);
        }
    }


    private void makeRestCall(String startPoint, String endPoint, Date date) {
        RestService service = RestRetriever.getClient().create(RestService.class);
        Call<List<TripList>> call = service.getTripList("trip_list", startPoint.trim(), endPoint.trim(), date.getTime());

        call.enqueue(new Callback<List<TripList>>() {
            @Override
            public void onResponse(Call<List<TripList>> call, Response<List<TripList>> response) {
                materialDialog.dismiss();
                List<TripList> tripList = response.body();
                recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                recyclerView.setAdapter(new TripAdapter(tripList, R.layout.list_item_trip, getActivity()));
            }

            @Override
            public void onFailure(Call<List<TripList>> call, Throwable t) {
                materialDialog.dismiss();
                Toast.makeText(getActivity(), R.string.connection_failure, Toast.LENGTH_SHORT).show();
                Log.e(TAG, "Search Fragment: Error: " + t.getMessage());
            }
        });

    }

    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel();
            checkIfFieldsAreFull();
        }

    };

    private void updateLabel() {
        String myFormat = "dd/MM/yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        pickDateButton.setText(sdf.format(myCalendar.getTime()));
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
    }
}
