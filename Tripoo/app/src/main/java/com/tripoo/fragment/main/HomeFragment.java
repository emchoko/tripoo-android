package com.tripoo.fragment.main;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Toast;

import com.tripoo.R;
import com.tripoo.rest.RestRetriever;
import com.tripoo.rest.RestService;
import com.tripoo.rest.adapter.BookingAdapter;
import com.tripoo.rest.adapter.EmptyAdapter;
import com.tripoo.rest.adapter.RouteAdapter;
import com.tripoo.rest.adapter.TripAdapter;
import com.tripoo.rest.model.get.details.RouteDetails;
import com.tripoo.rest.model.get.list.BookingList;
import com.tripoo.rest.model.get.list.RouteList;
import com.tripoo.rest.model.get.list.TripList;
import com.tripoo.utils.Constants;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by emil on 1/6/17.
 */

public class HomeFragment extends Fragment {

    private static final String TAG = HomeFragment.class.getSimpleName();

    @BindView(R.id.bookedTripsRecyclerView)
    public RecyclerView bookedRecyclerView;

    @BindView(R.id.upcomingTripsRecyclerView)
    public RecyclerView upcomingTripsRecyclerView;

    public List<TripList> tripList = new ArrayList<>();

    private Long id;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(Constants.userDetailsSharedPreferences, Context.MODE_PRIVATE);
        id = sharedPreferences.getLong(Constants.id, 0);

        receiveTrips(id, false);
        receiveTrips(id, true);
    }

    private void putBookedTripsIntoSharedPreferences(List<TripList> tripList, boolean isBookings) {
        Set<String> tripIds = new HashSet<>();
        for (TripList trip : tripList) {
            tripIds.add(trip.getId() + "");
        }

        SharedPreferences sharedPref = getActivity().getSharedPreferences(Constants.userDetailsSharedPreferences, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        if (isBookings)
            editor.putStringSet(Constants.bookings, tripIds);
        else
            editor.putStringSet(Constants.user_trips, tripIds);
        editor.apply();
        editor.commit();

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.home_layout, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    private void receiveTrips(Long userId, final boolean bookings) {
        RestService service = RestRetriever.getClient().create(RestService.class);

        if (bookings) {
            Call<List<BookingList>> call = service.getUserBookingsById(userId, "user_list_bookings");

            call.enqueue(new Callback<List<BookingList>>() {
                @Override
                public void onResponse(Call<List<BookingList>> call, Response<List<BookingList>> response) {
                    setUpBookingRecyclerView(response.body());
                }

                @Override
                public void onFailure(Call<List<BookingList>> call, Throwable t) {
                    Toast.makeText(getActivity(), "Failure!", Toast.LENGTH_SHORT).show();
                }
            });


        } else {
            Call<List<TripList>> call = service.getTripByUserId(userId, "trip_list");

            call.enqueue(new Callback<List<TripList>>() {
                @Override
                public void onResponse(Call<List<TripList>> call, Response<List<TripList>> response) {
                    setUpUpcomingRecyclerView(response.body());
                    putBookedTripsIntoSharedPreferences(tripList, false);
                }

                @Override
                public void onFailure(Call<List<TripList>> call, Throwable t) {
                    Toast.makeText(getActivity(), "Failure!", Toast.LENGTH_SHORT).show();
                }
            });
        }

    }


    private List<TripList> sortTripList(List<TripList> tripList) {
        List<TripList> result = new ArrayList<>();
        for (TripList trip : tripList) {
            if (convertStringToDate(trip.getDate()).after(new Date()))
                result.add(trip);
        }
        return result;
    }

    private Date convertStringToDate(String stringDate) {
        DateFormat format = new SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH);
        Date date = null;
        try {
            date = format.parse(stringDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Log.i(TAG, String.valueOf(date));
        return date;
    }

    private void setUpUpcomingRecyclerView(List<TripList> tripList) {
        tripList = sortTripList(tripList);
        upcomingTripsRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        if (tripList != null && tripList.size() > 0)
            upcomingTripsRecyclerView.setAdapter(new TripAdapter(tripList, R.layout.list_item_trip, getActivity()));
        else
            upcomingTripsRecyclerView.setAdapter(new EmptyAdapter("Your trips appear here!", R.layout.list_empty_trip));
    }


    private void setUpBookingRecyclerView(List<BookingList> bookings) {


        bookedRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        if (bookings != null && bookings.size() > 0) {
            bookedRecyclerView.setAdapter(new BookingAdapter(bookings, R.layout.booking_row_layout, getActivity()));
        } else {
            bookedRecyclerView.setAdapter(new EmptyAdapter("Your bookings appear here!", R.layout.list_empty_trip));
        }
    }


}
