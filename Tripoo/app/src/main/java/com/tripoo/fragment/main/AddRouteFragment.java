package com.tripoo.fragment.main;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.model.LatLng;
import com.tripoo.R;
import com.tripoo.rest.RestRetriever;
import com.tripoo.rest.RestService;
import com.tripoo.rest.model.post.request.Route;
import com.tripoo.rest.model.post.request.Trip;
import com.tripoo.utils.LocationHelper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static com.google.android.gms.location.places.AutocompleteFilter.TYPE_FILTER_CITIES;

/**
 * Created by emil on 1/15/17.
 */
public class AddRouteFragment extends Fragment {
    private static final String TAG = AddRouteFragment.class.getSimpleName();
    private static final int PLACE_AUTOCOMPLETE_REQUEST_CODE_START_POINT = 321;
    private static final int PLACE_AUTOCOMPLETE_REQUEST_CODE_END_POINT = 322;
    private static final int PLACE_AUTOCOMPLETE_REQUEST_CODE_MIDDLE_POINT = 323;

    private List<Route> routes;
    private Date tripDateAndTime;
    private Map<String, LatLng> places;
    private Map<String, Integer> routePrice;

    @BindView(R.id.routesLinearLayout)
    public ViewGroup linearLayout;

    @BindView(R.id.addRouteToLayout)
    public Button addViewToLayout;

    @BindView(R.id.addRouteStartPointBtn)
    public Button startPointBtn;

    @BindView(R.id.addRouteEndPointBtn)
    public Button endPointBtn;

    @BindView(R.id.createBtn)
    public Button createBtn;

    @BindView(R.id.removeFromPriceEndPointBtn)
    public ImageButton removeFromPriceEndPointBtn;

    @BindView(R.id.addToPriceEndPointBtn)
    public ImageButton addToPriceEndPointBtn;

    @BindView(R.id.priceEndPointTxtView)
    public TextView priceEndPointTxtView;

    @BindView(R.id.removeFromPriceTripBtn)
    public ImageButton removeFromTotalPrice;

    @BindView(R.id.addToPriceTripBtn)
    public ImageButton addToTotalPrice;

    @BindView(R.id.priceTripTxtView)
    public TextView totalTripPriceTxtView;

    private int endPointPrice = 0;
    private int totalTripPrice = 0;
    private int freePlaces;

    public Trip trip;
    public String token;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        trip = (Trip) getArguments().getSerializable("trip");
        freePlaces = getArguments().getInt("places");
        token = getArguments().getString("token");
        tripDateAndTime = new Date(getArguments().getLong("date"));

        places = new LinkedHashMap<>();
        routes = new ArrayList<>();
        routePrice = new HashMap<>();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.add_route_layout, container, false);
        ButterKnife.bind(this, view);
        setViewsVisibility(false);

        refreshTotalPriceTextView();
        priceEndPointTxtView.setText(endPointPrice + "");


        removeFromTotalPrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (totalTripPrice > 0)
                    totalTripPrice--;
                refreshTotalPriceTextView();
            }
        });

        addToTotalPrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                totalTripPrice++;
                refreshTotalPriceTextView();
            }
        });

        startPointBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchPlacesIntent(PLACE_AUTOCOMPLETE_REQUEST_CODE_START_POINT);
            }
        });

        endPointBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchPlacesIntent(PLACE_AUTOCOMPLETE_REQUEST_CODE_END_POINT);
            }
        });

        addViewToLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchPlacesIntent(PLACE_AUTOCOMPLETE_REQUEST_CODE_MIDDLE_POINT);
            }
        });

        addToPriceEndPointBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                endPointPrice++;
                totalTripPrice++;
                refreshTotalPriceTextView();
                priceEndPointTxtView.setText(endPointPrice + "");
                routePrice.put(endPointBtn.getText().toString(), endPointPrice);
            }
        });

        removeFromPriceEndPointBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (endPointPrice > 0) {
                    endPointPrice--;
                    totalTripPrice--;
                    refreshTotalPriceTextView();
                }
                priceEndPointTxtView.setText(endPointPrice + "");
                routePrice.put(endPointBtn.getText().toString(), endPointPrice);
            }
        });

        createBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createRoutes();
            }
        });

        return view;
    }

    private void setViewsVisibility(boolean visible) {
        int v;
        if (visible)
            v = View.VISIBLE;
        else
            v = View.INVISIBLE;

        addToPriceEndPointBtn.setVisibility(v);
        removeFromPriceEndPointBtn.setVisibility(v);
        priceEndPointTxtView.setVisibility(v);
        addViewToLayout.setVisibility(v);

    }

    private void createRoutes() {

        Map<String, Float> sortedFloatMap = sortByStartPoint(places);

        places = sortedRoutes(sortedFloatMap, places, startPointBtn.getText().toString(), endPointBtn.getText().toString());

        for (Map.Entry<String, LatLng> entry : places.entrySet()) {
            Log.d(TAG, entry.getKey() + " : " + entry.getValue());
        }

        List<String> list = new ArrayList<>(places.keySet());

        for (int i = 0; i < list.size() - 1; i++) {

            Route route = new Route();
            route.setStartPoint(list.get(i));
            route.setEndPoint(list.get(i + 1));

            route.setStartLatitude(places.get(list.get(i)).latitude);
            route.setStartLongitude(places.get(list.get(i)).longitude);

            route.setEndLatitude(places.get(list.get(i + 1)).latitude);
            route.setEndLongitude(places.get(list.get(i + 1)).longitude);

            route.setTotalFreePlaces(freePlaces);
            route.setCurrentFreePlaces(freePlaces);

            route.setStartTime(new Date().getTime());
            route.setEstimatedEndTime(new Date().getTime());

            route.setPrice(routePrice.get(route.getEndPoint()));
            routes.add(route);
        }

        trip.setRoutes(routes);
        trip.setPrice(totalTripPrice);
        callPostTrip();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE_START_POINT) {
            if (resultCode == RESULT_OK) {

                Place place = PlaceAutocomplete.getPlace(getActivity(), data);

                String startPoint = LocationHelper.convertLatLngToEnglishString(place.getLatLng(), getActivity());
                if (startPoint != null) {
                    startPointBtn.setText(startPoint);
                    setViewsVisibility(!endPointBtn.getText().toString().toLowerCase().equals("end point"));
                    places.put(startPoint, place.getLatLng());
                } else
                    LocationHelper.makeToastForProblemWithGeocoder(getActivity());


            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(getActivity(), data);
                Toast.makeText(getActivity(), status.getStatusMessage(), Toast.LENGTH_SHORT).show();
            }
        } else if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE_END_POINT) {
            if (resultCode == RESULT_OK) {

                Place place = PlaceAutocomplete.getPlace(getActivity(), data);

                String endPoint = LocationHelper.convertLatLngToEnglishString(place.getLatLng(), getActivity());
                if (endPoint != null) {
                    endPointBtn.setText(endPoint);
                    places.put(endPoint, place.getLatLng());
                    setViewsVisibility(!startPointBtn.getText().toString().toLowerCase().equals("start point"));
                } else
                    LocationHelper.makeToastForProblemWithGeocoder(getActivity());

            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(getActivity(), data);
                Toast.makeText(getActivity(), status.getStatusMessage(), Toast.LENGTH_SHORT).show();
            }
        } else if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE_MIDDLE_POINT) {
            if (resultCode == RESULT_OK) {

                Place place = PlaceAutocomplete.getPlace(getActivity(), data);
                String middlePoint = LocationHelper.convertLatLngToEnglishString(place.getLatLng(), getActivity());

                if (middlePoint != null) {
                    routePrice.put(middlePoint, 0);
                    addRouteLayout(getActivity(), middlePoint, tripDateAndTime.getHours() + ":" + tripDateAndTime.getMinutes(), 0);
                    places.put(middlePoint, place.getLatLng());
                } else
                    LocationHelper.makeToastForProblemWithGeocoder(getActivity());


            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(getActivity(), data);
                Toast.makeText(getActivity(), status.getStatusMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }


    private void launchPlacesIntent(int code) {
        try {
            AutocompleteFilter filterCities = new AutocompleteFilter.Builder()
                    .setTypeFilter(TYPE_FILTER_CITIES)
                    .build();


            Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                    .setFilter(filterCities)
                    .build(getActivity());
            startActivityForResult(intent, code);
        } catch (GooglePlayServicesRepairableException e) {
            Toast.makeText(getActivity(), R.string.google_play_services_repairable, Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            Toast.makeText(getActivity(), R.string.google_play_services_not_available, Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    private void addRouteLayout(Context context, final String city, String time, int price) {
        final View layout2 = LayoutInflater.from(context).inflate(R.layout.route_layout, linearLayout, false);

        TextView cityTxtView = (TextView) layout2.findViewById(R.id.cityTxtView);
        TextView timeTxtView = (TextView) layout2.findViewById(R.id.timeTxtView);
        Button removeRouteBtn = (Button) layout2.findViewById(R.id.removeBtn);
        ImageButton removeFromPrice = (ImageButton) layout2.findViewById(R.id.removeFromPriceBtn);
        ImageButton addToPrice = (ImageButton) layout2.findViewById(R.id.addToPriceBtn);
        final TextView priceTxtView = (TextView) layout2.findViewById(R.id.priceRouteTxtView);

        priceTxtView.setText(price + "");
        cityTxtView.setText(city);
        timeTxtView.setText(time);

        removeFromPrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int price = routePrice.get(city);
                if (price > 0) {
                    price--;
                    totalTripPrice--;
                    refreshTotalPriceTextView();
                }
                routePrice.put(city, price);
                priceTxtView.setText(price + "");
            }
        });

        addToPrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int price = routePrice.get(city);
                price++;
                totalTripPrice++;
                refreshTotalPriceTextView();
                routePrice.put(city, price);
                priceTxtView.setText(price + "");
            }
        });

        removeRouteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(), "Removeeed!!!", Toast.LENGTH_SHORT).show();
                places.remove(city);
                linearLayout.removeView(layout2);
            }
        });

        linearLayout.addView(layout2);
    }

    private void refreshTotalPriceTextView() {
        totalTripPriceTxtView.setText(totalTripPrice + "");
    }

    private float measureDistanceBetweenTwoPoints(LatLng firstPoint, LatLng secondPoint) {
        Location locationA = new Location("point A");
        locationA.setLatitude(firstPoint.latitude);
        locationA.setLongitude(firstPoint.longitude);

        Location locationB = new Location("point B");
        locationB.setLatitude(secondPoint.latitude);
        locationB.setLongitude(secondPoint.longitude);
        return locationA.distanceTo(locationB) / 1000;
    }

    private Map<String, Float> sortByStartPoint(Map<String, LatLng> map) {
        Map<String, Float> distanacedMap = new HashMap<>(dinstanceMap(map, startPointBtn.getText().toString(), endPointBtn.getText().toString()));

        List<Map.Entry<String, Float>> list = new LinkedList<>(distanacedMap.entrySet());
        Collections.sort(list, new Comparator<Map.Entry<String, Float>>() {
            @Override
            public int compare(Map.Entry<String, Float> entry1, Map.Entry<String, Float> entry2) {
                return (entry1.getValue()).compareTo(entry2.getValue());
            }
        });

        Map<String, Float> result = new LinkedHashMap<>();
        for (Map.Entry<String, Float> entry : list) {
            result.put(entry.getKey(), entry.getValue());
        }
        return result;
    }

    private Map<String, Float> dinstanceMap(Map<String, LatLng> map, String startPoint, String endPoint) {
        Map<String, Float> result = new HashMap<>();
        LatLng startPointLatLng = map.get(startPoint);
        for (Map.Entry<String, LatLng> entry : map.entrySet()) {
            if (!entry.getKey().equals(startPoint) && !entry.getKey().equals(endPoint)) {
                result.put(entry.getKey(), measureDistanceBetweenTwoPoints(startPointLatLng, entry.getValue()));
            }
        }

        return result;
    }

    private Map<String, LatLng> sortedRoutes(Map<String, Float> parent, Map<String, LatLng> child, String startPoint, String endPoint) {
        Map<String, LatLng> result = new LinkedHashMap<>();
        result.put(startPoint, child.get(startPoint));
        for (Map.Entry<String, Float> entry : parent.entrySet()) {
            result.put(entry.getKey(), child.get(entry.getKey()));
        }
        result.put(endPoint, child.get(endPoint));
        return result;
    }

    private void callPostTrip() {
        RestService service = RestRetriever.getClient().create(RestService.class);
        Call<Trip> call = service.addTrip(trip, token);

        Log.d(TAG, "Trip: " + trip.toString()
                + "\nToken: " + token);

        call.enqueue(new Callback<Trip>() {
            @Override
            public void onResponse(Call<Trip> call, Response<Trip> response) {
                Toast.makeText(getActivity(), response.body().toString(), Toast.LENGTH_SHORT).show();
                Toast.makeText(getActivity(), "Must be added!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<Trip> call, Throwable t) {
                Toast.makeText(getActivity(), R.string.connection_failure, Toast.LENGTH_SHORT).show();
                Log.d(TAG, "Localized Message: " + t.getLocalizedMessage()
                        + "\nMessage" + t.getMessage()
                        + "\nCause: " + t.getCause()
                        + "\nStackTrace: " + t.getStackTrace().toString());
            }
        });
    }

}
