package com.tripoo.fragment.messaging;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.sendbird.android.BaseChannel;
import com.sendbird.android.BaseMessage;
import com.sendbird.android.GroupChannel;
import com.sendbird.android.PreviousMessageListQuery;
import com.sendbird.android.SendBird;
import com.sendbird.android.SendBirdException;
import com.sendbird.android.UserMessage;
import com.tripoo.R;
import com.tripoo.messaging.adapter.MessagingAdapter;
import com.tripoo.utils.Helper;
import com.tripoo.utils.UiShortcuts;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by emil on 1/22/17.
 */

public class ChatRoomFragment extends Fragment {
    private static final String TAG = ChatRoomFragment.class.getSimpleName();
    private static final String INDENTIFIER = "1458451931";
    private String channelUrl;

    @BindView(R.id.list)
    public ListView listView;

    @BindView(R.id.btn_send)
    public Button sendBtn;

    @BindView(R.id.etxt_message)
    public EditText messageEdtTxt;


    private GroupChannel mGroupChannel;
    private PreviousMessageListQuery previousMessageListQuery;
    private MessagingAdapter adapter;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.chat_room_fragment, container, false);
        ButterKnife.bind(this, view);
        channelUrl = getArguments().getString(getActivity().getString(R.string.channel_url));
        initUiComponents();
        initGroupChannel();

        return view;
    }

    private void initGroupChannel() {
        GroupChannel.getChannel(channelUrl, new GroupChannel.GroupChannelGetHandler() {
            @Override
            public void onResult(GroupChannel groupChannel, SendBirdException e) {
                if (e != null) {
                    Toast.makeText(getActivity(), "" + e.getCode() + ":" + e.getMessage(), Toast.LENGTH_SHORT).show();
                    Log.e(TAG, "Loading Previous Messages Error: "
                            + "\nCode: " + e.getCode()
                            + "\nMessage: " + e.getMessage()
                            + "\nCause: " + e.getCause()
                            + "\nLocalized message: " + e.getLocalizedMessage());
                    return;
                }

                mGroupChannel = groupChannel;
                mGroupChannel.markAsRead();

                adapter = new MessagingAdapter(getActivity(), mGroupChannel);
                listView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                loadPreviousMessages(true);
            }
        });
    }

    private void refreshGroupChannel() {
        if (mGroupChannel == null) return;

        mGroupChannel.markAsRead();
        mGroupChannel.refresh(new GroupChannel.GroupChannelRefreshHandler() {
            @Override
            public void onResult(SendBirdException e) {
                adapter.notifyDataSetChanged();
                loadPreviousMessages(true);
            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
        SendBird.removeChannelHandler(INDENTIFIER);
    }


    @Override
    public void onResume() {
        super.onResume();
        SendBird.addChannelHandler(INDENTIFIER, new SendBird.ChannelHandler() {
            @Override
            public void onMessageReceived(BaseChannel baseChannel, BaseMessage baseMessage) {
                if (adapter != null){
                    mGroupChannel.markAsRead();
                    adapter.appendMessage(baseMessage);
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onReadReceiptUpdated(GroupChannel channel) {
                if(mGroupChannel.getUrl().equals(channelUrl))
                    adapter.notifyDataSetChanged();
            }
        });
        refreshGroupChannel();
    }

    private void initUiComponents() {
        sendBtn.setEnabled(false);
        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                send();
            }
        });

        messageEdtTxt.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    if (keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
                        send();
                    }
                    return true;
                }
                return false;
            }
        });

        messageEdtTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                sendBtn.setEnabled(s.length() > 0);

                if (s.length() == 1) {
                    mGroupChannel.startTyping();
                } else if (s.length() < 1) {
                    mGroupChannel.endTyping();
                    //TODO:check if this works
                    sendBtn.setEnabled(false);
                }
            }
        });


        listView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                UiShortcuts.hideKeyboard(getActivity());
                return false;
            }
        });

        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int state) {
                if (state == SCROLL_STATE_IDLE)
                    if (view.getFirstVisiblePosition() == 0 && view.getChildCount() > 0 && view.getChildAt(0).getTop() == 0)
                        loadPreviousMessages(false);
            }

            @Override
            public void onScroll(AbsListView absListView, int i, int i1, int i2) {
            }
        });

    }

    private void loadPreviousMessages(final boolean refresh) {
        if (mGroupChannel == null)
            return;

        if (refresh || previousMessageListQuery == null)
            previousMessageListQuery = mGroupChannel.createPreviousMessageListQuery();

        if (previousMessageListQuery.isLoading() || !previousMessageListQuery.hasMore())
            return;

        previousMessageListQuery.load(30, true, new PreviousMessageListQuery.MessageListQueryResult() {
            @Override
            public void onResult(List<BaseMessage> list, SendBirdException e) {
                if (e != null) {
                    Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    Log.e(TAG, "Loading Previous Messages Error: "
                            + "\nCode: " + e.getCode()
                            + "\nMessage: " + e.getMessage()
                            + "\nCause: " + e.getCause()
                            + "\nLocalized message: " + e.getLocalizedMessage());
                }

                if (refresh)
                    adapter.clear();

                for (BaseMessage baseMessage : list)
                    adapter.insertMessage(baseMessage);

                adapter.notifyDataSetChanged();
                listView.setSelection(list.size());
            }
        });
    }


    private void send() {
        if (messageEdtTxt.getText().length() < 1) {
            return;
        }

        mGroupChannel.sendUserMessage(messageEdtTxt.getText().toString(), new BaseChannel.SendUserMessageHandler() {
            @Override
            public void onSent(UserMessage userMessage, SendBirdException e) {
                if (e != null) {
                    Toast.makeText(getActivity(), "" + e.getCode() + ":" + e.getMessage(), Toast.LENGTH_SHORT).show();
                    return;
                }

                adapter.appendMessage(userMessage);
                adapter.notifyDataSetChanged();

                messageEdtTxt.setText("");
            }
        });
    }


}