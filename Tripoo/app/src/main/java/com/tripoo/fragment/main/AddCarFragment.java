package com.tripoo.fragment.main;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.tripoo.R;
import com.tripoo.rest.RestRetriever;
import com.tripoo.rest.RestService;
import com.tripoo.rest.model.get.details.CarDetails;
import com.tripoo.rest.model.post.request.CarDetailsNoId;
import com.tripoo.utils.Constants;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by emil on 1/28/17.
 */
public class AddCarFragment extends Fragment {


    private static final String TAG = AddCarFragment.class.getSimpleName();
    @BindView(R.id.addCarManufacturer)
    EditText manufacturerTxtView;
    private boolean isManufacturerEmpty = true;

    @BindView(R.id.addCarModel)
    EditText modelTxtView;
    private boolean isModelEmpty = true;

    @BindView(R.id.addCarYearBtn)
    Button yearBtn;
    private boolean isYearEmpty = true;


    @BindView(R.id.addCarComfortLevel)
    Button comfortBtn;
    private boolean isComfortEmpty = true;

    @BindView(R.id.createCarBtn)
    Button createBtn;


    private String token;
    private List<Integer> years = new ArrayList<>();
    private List<Integer> levels = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.add_car_layout, container, false);
        ButterKnife.bind(this, view);

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(Constants.userDetailsSharedPreferences, Context.MODE_PRIVATE);
        token = sharedPreferences.getString(Constants.token, "");

        initViews();
        return view;
    }


    private void initViews() {
        createBtn.setEnabled(false);

        manufacturerTxtView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                isManufacturerEmpty = editable.length() <= 0;
                shouldCreateBeEnabled();
            }
        });

        modelTxtView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                isModelEmpty = editable.length() <= 0;
                shouldCreateBeEnabled();
            }
        });

        yearBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                for (int i = Calendar.getInstance().getTime().getYear() + 1900; i > 1900; i--) {
                    years.add(i);
                }

                Log.d(TAG, "List Years Size: " + years.size() + "; year: " + Calendar.getInstance().getTime().getYear());
                MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                        .title("Pick year:")
                        .positiveText("Choose")
                        .positiveColorRes(R.color.colorPrimary)
                        .itemsColorRes(R.color.colorPrimary)
                        .items(years)
                        .itemsCallbackSingleChoice(-1, new MaterialDialog.ListCallbackSingleChoice() {
                            @Override
                            public boolean onSelection(MaterialDialog dialog, View itemView, int which, CharSequence text) {
                                yearBtn.setText(years.get(which) + "");
                                isYearEmpty = false;
                                return true;
                            }
                        })
                        .show();
                shouldCreateBeEnabled();
            }
        });

        comfortBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (int i = 1; i <= 5; i++) {
                    levels.add(i);
                }

                MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                        .title("Pick Comfort Level:")
                        .positiveText("Choose")
                        .positiveColorRes(R.color.colorPrimary)
                        .itemsColorRes(R.color.colorPrimary)
                        .items(levels)
                        .itemsCallbackSingleChoice(-1, new MaterialDialog.ListCallbackSingleChoice() {
                            @Override
                            public boolean onSelection(MaterialDialog dialog, View itemView, int which, CharSequence text) {
                                comfortBtn.setText(levels.get(which) + "");
                                isComfortEmpty = false;
                                return true;
                            }
                        })
                        .show();

                shouldCreateBeEnabled();
            }
        });

        createBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CarDetailsNoId car = new CarDetailsNoId();
                car.setManufacturer(manufacturerTxtView.getText().toString());
                car.setModel(modelTxtView.getText().toString());
                car.setYear(Integer.parseInt(yearBtn.getText().toString()));
                car.setComfortLevel(Integer.parseInt(comfortBtn.getText().toString()));

                postCarToService(car);
            }
        });
    }

    private void shouldCreateBeEnabled() {
        if (!isManufacturerEmpty && !isModelEmpty && !isYearEmpty && !isComfortEmpty)
            createBtn.setEnabled(true);
    }

    private void postCarToService(CarDetailsNoId carDetailsNoId) {
        RestService service = RestRetriever.getClient().create(RestService.class);
        Call<CarDetails> call = service.createCar(carDetailsNoId, token);

        call.enqueue(new Callback<CarDetails>() {
            @Override
            public void onResponse(Call<CarDetails> call, Response<CarDetails> response) {
                getActivity().finish();
            }

            @Override
            public void onFailure(Call<CarDetails> call, Throwable t) {
                Toast.makeText(getActivity(), "Error Creating Car!", Toast.LENGTH_SHORT).show();
                Log.e(TAG, "Error: " + t.getMessage());
            }
        });
    }
}
