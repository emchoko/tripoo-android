package com.tripoo.fragment.main;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tripoo.R;
import com.tripoo.rest.RestRetriever;
import com.tripoo.rest.RestService;
import com.tripoo.rest.adapter.AlertAdapter;
import com.tripoo.rest.model.post.request.Alert;
import com.tripoo.utils.Constants;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by emil on 2/14/17.
 */
public class AlertsFragment extends Fragment {

    @BindView(R.id.alertRecyclerView)
    RecyclerView recyclerView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.alert_layout, container, false);
        ButterKnife.bind(this, view);

        SharedPreferences prefs = getActivity().getSharedPreferences(Constants.userDetailsSharedPreferences, Context.MODE_PRIVATE);
        Long userId = prefs.getLong(Constants.id, 0);

        makeRestCall(userId);
        return view;
    }

    private void makeRestCall(Long userId) {
        RestService service = RestRetriever.getClient().create(RestService.class);
        Call<List<Alert>> call = service.getAlertsByUserId(userId, "alert_list");

        call.enqueue(new Callback<List<Alert>>() {
            @Override
            public void onResponse(Call<List<Alert>> call, Response<List<Alert>> response) {
                setUpRecyclerView(response.body());
            }

            @Override
            public void onFailure(Call<List<Alert>> call, Throwable t) {

            }
        });
    }

    private void setUpRecyclerView(List<Alert> body) {
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(new AlertAdapter(R.layout.alert_row_layout, body, getActivity()));
    }
}
