package com.tripoo.fragment.main;

import android.Manifest;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.squareup.picasso.Picasso;
import com.tripoo.R;
import com.tripoo.rest.RestRetriever;
import com.tripoo.rest.RestService;
import com.tripoo.rest.UploadService;
import com.tripoo.rest.model.get.profile.UserProfile;
import com.tripoo.rest.model.post.request.UploadImage;
import com.tripoo.utils.Constants;
import com.tripoo.utils.DocumentHelper;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

/**
 * Created by emil on 1/6/17.
 */
public class ProfileFragment extends Fragment {

    private static final String TAG = ProfileFragment.class.getSimpleName();
    //TODO: fix the name
    private static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 621;

    private UserProfile user;

    @BindView(R.id.usernameTxtView)
    public TextView usernameTextView;

    @BindView(R.id.firstLastNameTxtView)
    public TextView firstLastNameTextView;

    @BindView(R.id.userEmailTxtView)
    public TextView userEmailTextView;

    @BindView(R.id.ratingTextView)
    public TextView ratingTextView;

//    @BindView(R.id.facebookLink)
//    public TextView facebookTextView;

    @BindView(R.id.phoneNumber)
    public TextView phoneTextView;

    @BindView(R.id.userProfilePhoto)
    public ImageView profilePhoto;

    private Long userId;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.user_profile, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences preferences = getActivity().getSharedPreferences(Constants.userDetailsSharedPreferences, Context.MODE_PRIVATE);
        userId = preferences.getLong(Constants.id, 0);
        makeRestCall(userId);
    }

    private void makeRestCall(final Long userId) {
        RestService service = RestRetriever.getClient().create(RestService.class);
        Call<UserProfile> call = service.getUserById(userId, "user_profile");
        Log.i(TAG, "UserID: " + userId);

        call.enqueue(new Callback<UserProfile>() {
            @Override
            public void onResponse(Call<UserProfile> call, Response<UserProfile> response) {
                if (response.body() != null) {
                    user = response.body();
                    loadViews();
                }
            }

            @Override
            public void onFailure(Call<UserProfile> call, Throwable t) {
                Log.d("TAG", "Failure: " + t.getMessage());
                Toast.makeText(getActivity(), R.string.connection_failure, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Uri returnUri;
        if (requestCode != Constants.FILE_PICK || resultCode != RESULT_OK)
            return;

        returnUri = data.getData();
        String filePath = DocumentHelper.getPath(getActivity(), returnUri);
        if (filePath == null || filePath.isEmpty()) return;
        File chosenFile = new File(filePath);

        UploadImage upload = new UploadImage();
        upload.image = chosenFile;
        upload.title = user.getUsername() + " profilePhoto";
        upload.uri = returnUri;
        Picasso.with(getActivity()).load(returnUri).into(profilePhoto);
        UploadService service = new UploadService(getContext(), getActivity());
        service.buildRestAdapter(upload, userId);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_CONTACTS:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    filePicker();
                } else {
                    new MaterialDialog.Builder(getActivity())
                            .title("Permissions needed!")
                            .content("App needs permissions to access storage")
                            .positiveText("OK")
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    requestPermission();
                                }
                            })
                            .show();
                }
        }
    }

    private void loadViews() {
        usernameTextView.setText(user.getUsername());
        firstLastNameTextView.setText(user.getFirstName() + " " + user.getLastName());
        userEmailTextView.setText(user.getEmail());
        ratingTextView.setText(user.getRating().toString());
//        facebookTextView.setText(user.getLinkToFacebook());
        phoneTextView.setText(user.getPhoneNumber());
        if (user.getPhotoUrl() == null)
            Picasso.with(getActivity()).load(R.drawable.ic_person_black_24dp).into(profilePhoto);
        else
            Picasso.with(getActivity()).load(user.getPhotoUrl()).into(profilePhoto);

        profilePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    filePicker();
                } else {

                    if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        new MaterialDialog.Builder(getActivity())
                                .title("Permissions needed!")
                                .content("App needs permissions to access storage")
                                .positiveText("OK")
                                .onPositive(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                        requestPermission();
                                    }
                                })
                                .show();
                    } else {
                        requestPermission();
                    }

                }
            }
        });
    }


    private void requestPermission() {
        ActivityCompat.requestPermissions(getActivity(),
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                MY_PERMISSIONS_REQUEST_READ_CONTACTS);
    }

    private void filePicker() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(intent, Constants.FILE_PICK);
    }
}
