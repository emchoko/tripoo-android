package com.tripoo;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.squareup.picasso.Picasso;
import com.tripoo.activities.AddCarActivity;
import com.tripoo.activities.AddTripActivity;
import com.tripoo.activities.MessagingActivity;
import com.tripoo.activities.SearchActivity;
import com.tripoo.fragment.main.AlertsFragment;
import com.tripoo.fragment.main.CarFragment;
import com.tripoo.fragment.main.HomeFragment;
import com.tripoo.fragment.main.ProfileFragment;
import com.tripoo.rest.RestRetriever;
import com.tripoo.rest.RestService;
import com.tripoo.rest.model.get.profile.UserProfile;
import com.tripoo.rest.model.post.response.Token;
import com.tripoo.services.InstanceIdService;
import com.tripoo.utils.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = MainActivity.class.getSimpleName();
    @BindView(R.id.toolbar)
    public Toolbar toolbar;

    @BindView(R.id.drawer_layout)
    public DrawerLayout drawer;

    private String token;
    private String username;
    private String password;
    private long userId;

    private android.app.FragmentManager manager = getFragmentManager();
    private TextView headerViewNames;
    private TextView headerViewEmail;
    private ImageView headerViewPicture;
    private FloatingActionButton floatingActionButton;

    private boolean isOnHome = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        SharedPreferences preferences = getSharedPreferences(Constants.userDetailsSharedPreferences, Context.MODE_PRIVATE);
        userId = preferences.getLong(Constants.id, 0);

        token = getIntent().getStringExtra(Constants.token);
        username = getIntent().getStringExtra(Constants.username);
        password = getIntent().getStringExtra(Constants.password);

        floatingActionButton = (FloatingActionButton) findViewById(R.id.mainFab);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isOnHome) {
                    Intent intent = new Intent(MainActivity.this, AddTripActivity.class);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(MainActivity.this , AddCarActivity.class);
                    startActivity(intent);
                }
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        setUpHomeFragment();


        Log.i(TAG, "Firebase Token: " + FirebaseInstanceId.getInstance().getToken() );
        sendRegistrationTokenToREST(FirebaseInstanceId.getInstance().getToken());

        final ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();


        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        headerViewNames = (TextView) headerView.findViewById(R.id.userNames);
        headerViewEmail = (TextView) headerView.findViewById(R.id.userEmail);
        headerViewPicture = (ImageView) headerView.findViewById(R.id.profilePicture);
        navigationView.setNavigationItemSelectedListener(this);

        setUpHeaderView();
    }

    public void sendRegistrationTokenToREST(final String refreshedToken) {
        SharedPreferences sharedPreferences = getSharedPreferences(Constants.userDetailsSharedPreferences, Context.MODE_PRIVATE);
        String token = sharedPreferences.getString(Constants.token, "");
        long userId = sharedPreferences.getLong(Constants.id, 0);

        RestService service = RestRetriever.getClient().create(RestService.class);
        Token body = new Token();
        body.setToken(refreshedToken);

        Call<String> call = service.patchToken(token, userId, body);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                Log.i(TAG, "Refreshed token!");
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.e(TAG, "Not refreshed token!");
            }
        });
    }

    private void setUpHeaderView() {
        RestService service = RestRetriever.getClient().create(RestService.class);
        Call<UserProfile> call = service.getUserById(userId, "user_profile");

        call.enqueue(new Callback<UserProfile>() {
            @Override
            public void onResponse(Call<UserProfile> call, Response<UserProfile> response) {
                headerViewNames.setText(response.body().getFirstName() + " " + response.body().getLastName());
                headerViewEmail.setText(response.body().getEmail());

                if(response.body().getPhotoUrl() == null)
                    Picasso.with(MainActivity.this).load(R.drawable.ic_person_black_24dp).into(headerViewPicture);
                else
                    Picasso.with(MainActivity.this).load(response.body().getPhotoUrl()).into(headerViewPicture);

            }

            @Override
            public void onFailure(Call<UserProfile> call, Throwable t) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        switch (item.getItemId()) {
            case R.id.toolbarSearch:
                Toast.makeText(this, "Search", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(this, SearchActivity.class));
                break;
            case R.id.toolbarMessaging:
                Toast.makeText(this, "MessagingActivity", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(this, MessagingActivity.class);
                intent.putExtra(Constants.username, username);
                intent.putExtra(Constants.id, userId);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        final android.app.FragmentManager manager = getFragmentManager();

        switch (item.getItemId()) {
            case R.id.nav_home:
                isOnHome = true;
                if (floatingActionButton.getVisibility() == View.GONE)
                    floatingActionButton.setVisibility(View.VISIBLE);
                setUpHomeFragment();

                break;
            case R.id.nav_profile:
                floatingActionButton.setVisibility(View.GONE);
                ProfileFragment profileFragment = new ProfileFragment();
                manager
                        .beginTransaction()
                        .replace(R.id.mainFrameLayout,
                                profileFragment)
                        .commit();
                toolbar.setTitle("Profile");
                break;
            case R.id.nav_alerts:
                floatingActionButton.setVisibility(View.GONE);

                AlertsFragment alertsFragment= new AlertsFragment();
                manager
                        .beginTransaction()
                        .replace(R.id.mainFrameLayout,
                                alertsFragment)
                        .commit();
                toolbar.setTitle("Alerts");
                break;
            case R.id.nav_cars:
                isOnHome = false;
                if (floatingActionButton.getVisibility() == View.GONE) {
                    floatingActionButton.setVisibility(View.VISIBLE);
                }
                CarFragment carFragment = new CarFragment();
                manager.beginTransaction().replace(R.id.mainFrameLayout, carFragment).commit();
                toolbar.setTitle("Cars");
                break;

            case R.id.sign_out:
                finish();
                break;
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void setUpHomeFragment() {
        HomeFragment homeFragment = new HomeFragment();
        manager.beginTransaction().replace(R.id.mainFrameLayout, homeFragment).commit();
        toolbar.setTitle("Home");
    }

}