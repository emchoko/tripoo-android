package com.tripoo.utils;

/**
 * Created by emil on 1/26/17.
 */

public interface Constants {
    String username = "username";
    String password = "password";
    String token = "token";
    String id = "id";
    String trip_id = "trip_id";
    String userDetailsSharedPreferences = "UserDetails";
    String bookings = "bookings_for_user";
    String MY_IMGUR_CLIENT_ID = "3232a3f8eb9e757";
    String MY_IMGUR_CLIENT_SECRET = "a68a53e04b235a29ecff23d5bc9a4bd526416fb9";

    String CLIENT_AUTH = "Client-ID " + MY_IMGUR_CLIENT_ID;
    /*
      Redirect URL for android.
     */
    String MY_IMGUR_REDIRECT_URL = "http://android";
    String user_trips = "user_trips";

    int FILE_PICK = 7891;
}
