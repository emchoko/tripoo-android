package com.tripoo.utils;

import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by emil on 1/18/17.
 */

public class NumberToMonthConverter {

    public static String convert(int month) {
        switch (month) {
            case 1:
                return "January";
            case 2:
                return "February";
            case 3:
                return "March";
            case 4:
                return "April";
            case 5:
                return "May";
            case 6:
                return "June";
            case 7:
                return "July";
            case 8:
                return "August";
            case 9:
                return "September";
            case 10:
                return "October";
            case 11:
                return "November";
            case 12:
                return "December";
            default:
                return null;

        }
    }

    public static String convertDate(String date) {
        String[] list = date.split("-");
        String month = convert(Integer.parseInt(list[1]));
        return list[0] + " " + month + " " + list[2];
    }

    public static String formatDate(Date date) {
        String myFormat = "d-MM-yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        return sdf.format(date);
    }

}
