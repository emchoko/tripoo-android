package com.tripoo.utils;

import android.app.Activity;
import android.view.inputmethod.InputMethodManager;

/**
 * Created by emil on 1/22/17.
 */

public class UiShortcuts {

    public static void hideKeyboard(Activity activity) {
        if (activity == null || activity.getCurrentFocus() == null)
            return;

        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null)
            inputMethodManager.hideSoftInputFromInputMethod(activity.getCurrentFocus().getWindowToken(), 0);
    }
}
