package com.tripoo.utils;

import android.app.Activity;
import android.location.Address;
import android.location.Geocoder;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * Created by emil on 2/5/17.
 */

public class LocationHelper {


    private static final String TAG = LocationHelper.class.getSimpleName();

    public static String convertLatLngToEnglishString(LatLng latLng, Activity activity) {
        Geocoder geocoder = new Geocoder(activity, Locale.UK);
        try {
            List<Address> addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
            if (addresses.size() > 0) {
                Log.e(TAG, "Converted: " + addresses.get(0).getLocality());
                return addresses.get(0).getLocality();
            } else {
                Log.e(TAG, "empty");
                return null;
            }
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void makeToastForProblemWithGeocoder(Activity activity) {
        Toast.makeText(activity, "An error occurred with location. Please restart device in order to work!", Toast.LENGTH_SHORT).show();
    }

}
