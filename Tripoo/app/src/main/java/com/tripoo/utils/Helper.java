package com.tripoo.utils;

import android.content.Context;
import android.text.format.DateFormat;
import android.util.Log;

import com.sendbird.android.User;

import java.util.Date;
import java.util.List;

/**
 * Created by emil on 1/22/17.
 */

public class Helper {
    public static String getDisplayDateTime(Context context, long createdAt) {
        Date date = new Date(createdAt);

        if (System.currentTimeMillis() - createdAt > 60 * 60 * 24 * 1000l) {
            return DateFormat.getDateFormat(context).format(date);
        } else {
            return DateFormat.getTimeFormat(context).format(date);
        }
    }

    public static String convertMembersToString(List<User> list, String myId) {
        String res = "";
        for (User user : list) {
            if (!user.getUserId().equals(myId))
                res += user.getNickname() + " ";
        }
        return res;
    }
}
