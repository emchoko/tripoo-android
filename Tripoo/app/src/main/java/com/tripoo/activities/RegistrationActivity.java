package com.tripoo.activities;

import android.app.DatePickerDialog;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.tripoo.R;
import com.tripoo.rest.RestRetriever;
import com.tripoo.rest.RestService;
import com.tripoo.rest.model.post.request.UserRegistration;
import com.tripoo.utils.NumberToMonthConverter;

import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistrationActivity extends AppCompatActivity {


    private static final String TAG = RegistrationActivity.class.getSimpleName();
    @BindView(R.id.regUsernameEditText)
    EditText usernameEditText;
    @BindView(R.id.regPasswordEditText)
    EditText passEditText;
    @BindView(R.id.regEmailEditText)
    EditText emailEditText;
    @BindView(R.id.firstNameEditText)
    EditText firstNameEditText;
    @BindView(R.id.lastNameEditText)
    EditText lastNameEditText;
    @BindView(R.id.phoneNumberEditText)
    EditText phoneEditText;
    @BindView(R.id.birthDateButton)
    Button birthDateBtn;
    @BindView(R.id.signUpButton)
    Button signUpButton;

    private boolean isUsernameEmpty = true;
    private boolean isPassEmpty = true;
    private boolean isEmailEmpty = true;
    private boolean isFirstEmpty = true;
    private boolean isLastEmpty = true;
    private boolean isPhoneEmpty = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registation);
        ButterKnife.bind(this);

        Window window = this.getWindow();
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimaryDark));
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.registrationToolbar);
        toolbar.setTitle("Sign Up");
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorWhite));
        setSupportActionBar(toolbar);


        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(RegistrationActivity.this, "Finishing!", Toast.LENGTH_SHORT).show();
                finish();
            }
        });

        signUpButton.setEnabled(false);

    }


    @OnTextChanged(value = R.id.regUsernameEditText, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    public void onTextUsernameChanged(Editable editable) {
        if (editable.length() > 4) {
            isUsernameEmpty = false;
        } else {
            isUsernameEmpty = true;
            usernameEditText.setError("required");
        }
        enableSignUpButton();
    }


    @OnTextChanged(value = R.id.regPasswordEditText, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    public void onTextPasswordChanged(Editable editable) {
        if (editable.length() > 4) {
            isPassEmpty = false;
        } else {
            isPassEmpty = true;
            passEditText.setError("5 characters at least");
        }
        enableSignUpButton();
    }

    @OnTextChanged(value = R.id.regEmailEditText, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    public void onTextEmailChanged(Editable editable) {

        if (editable.length() > 4 && validateEmail(editable.toString())) {
            isEmailEmpty = false;
        } else {
            isEmailEmpty = true;
            emailEditText.setError("not valid");
        }
        enableSignUpButton();
    }

    @OnTextChanged(value = R.id.firstNameEditText, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    public void onTextFirstNameChanged(Editable editable) {
        if (editable.length() > 1) {
            isFirstEmpty = false;
        } else {
            isFirstEmpty = true;
            firstNameEditText.setError("not valid");
        }
        enableSignUpButton();
    }

    @OnTextChanged(value = R.id.lastNameEditText, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    public void onTextLastNameChanged(Editable editable) {
        if (editable.length() > 1) {
            isLastEmpty = false;
        } else {
            isLastEmpty = true;
            lastNameEditText.setError("not valid");
        }
        enableSignUpButton();
    }

    @OnTextChanged(value = R.id.phoneNumberEditText, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    public void onTextPhoneChanged(Editable editable) {
        if (editable.length() > 1) {
            isPhoneEmpty = false;
        } else {
            isPhoneEmpty = true;
            phoneEditText.setError("not valid");
        }
        enableSignUpButton();
    }

    private Calendar myCalendar = Calendar.getInstance();
    private long pickedDate;

    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker datePicker, int year, int monthOfYear,
                              int dayOfMonth) {

            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            pickedDate = myCalendar.getTime().getTime();
            updateLabel();
        }

    };

    private void updateLabel() {
        birthDateBtn.setText(NumberToMonthConverter.convertDate(NumberToMonthConverter.formatDate(myCalendar.getTime())));
        enableSignUpButton();
    }


    @OnClick(R.id.birthDateButton)
    public void onPickUpDateClick() {
        new DatePickerDialog(RegistrationActivity.this, date, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }


    public static final Pattern ptr =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    private boolean validateEmail(String s) {
        Matcher matcher = ptr.matcher(s);
        return matcher.find();
    }

    private void enableSignUpButton() {
        if (!isUsernameEmpty && !isPassEmpty && !isEmailEmpty && !isFirstEmpty && !isLastEmpty && !isPhoneEmpty && !birthDateBtn.getText().toString().equals(getResources().getString(R.string.birth_date))) {
            signUpButton.setEnabled(true);
        }
    }

    private MaterialDialog dialog;

    @OnClick(R.id.signUpButton)
    public void onSignUpClick() {
        dialog = new MaterialDialog.Builder(this)
                .title("Registration")
                .content("Building your profile ...")
                .progress(true, 0)
                .show();
        makeRegistrationCall();
    }


    private void makeRegistrationCall() {
        RestService service = RestRetriever.getClient().create(RestService.class);
        UserRegistration userRegistration = new UserRegistration(usernameEditText.getText().toString(),
                                                                 passEditText.getText().toString(),
                firstNameEditText.getText().toString(),
                lastNameEditText.getText().toString(),
                emailEditText.getText().toString(),
                pickedDate,
                phoneEditText.getText().toString());


        Call<UserRegistration> call = service.registrationOfUser(userRegistration);

        call.enqueue(new Callback<UserRegistration>() {
            @Override
            public void onResponse(Call<UserRegistration> call, Response<UserRegistration> response) {
                dialog.dismiss();
                switch (response.code()){
                    case 200:
                        Toast.makeText(RegistrationActivity.this, "Registration success!", Toast.LENGTH_SHORT).show();
                        finish();
                        break;
                    default:
                        Toast.makeText(RegistrationActivity.this, "Something went wrong!", Toast.LENGTH_SHORT).show();

                }
                Log.d(TAG, response.raw().toString());
            }

            @Override
            public void onFailure(Call<UserRegistration> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(RegistrationActivity.this, "Registration not successful!", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
