package com.tripoo.activities;

import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.TextView;

import com.tripoo.R;
import com.tripoo.fragment.messaging.ChatRoomFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChatRoomActivity extends AppCompatActivity {

    @BindView(R.id.backToActivityBtn)
    ImageButton buttonClose;

    @BindView(R.id.channelNameTextView)
    public TextView channelNameTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_room);
        ButterKnife.bind(this);
        Window window = this.getWindow();
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }

        initFragment();

        String name = getIntent().getExtras().getString("channel_name");
        if (name != null)
            channelNameTextView.setText(name);
        else
            channelNameTextView.setText("Not found!");

        buttonClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void initFragment() {
        ChatRoomFragment messagingFragment = new ChatRoomFragment();
        messagingFragment.setArguments(getIntent().getExtras());
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, messagingFragment).commit();
    }

}
