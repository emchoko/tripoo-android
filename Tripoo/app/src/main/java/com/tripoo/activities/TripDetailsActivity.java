package com.tripoo.activities;

import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.tripoo.R;
import com.tripoo.fragment.main.TripFragment;
import com.tripoo.utils.Constants;

public class TripDetailsActivity extends AppCompatActivity {

    private Long tripID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trip_details);

        tripID = getIntent().getLongExtra(Constants.trip_id, 0);

        Toolbar toolbar = (Toolbar) findViewById(R.id.carToolbar);
        toolbar.setTitle("Trip");
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_white_24dp));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        Window window = this.getWindow();
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimaryDark));
        }
        setUpTripFragment();
    }

    public void setUpTripFragment() {
        android.app.FragmentManager manager = getFragmentManager();
        TripFragment tripFragment = new TripFragment();
        Bundle arguments = new Bundle();
        arguments.putLong("trip_id", tripID);
        tripFragment.setArguments(arguments);
        manager.beginTransaction().replace(R.id.carActivityFrameLayout, tripFragment).commit();
    }
}
