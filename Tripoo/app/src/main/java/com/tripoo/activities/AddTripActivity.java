package com.tripoo.activities;

import android.app.FragmentManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.tripoo.R;
import com.tripoo.fragment.main.AddTripFragment;
import com.tripoo.utils.Constants;

public class AddTripActivity extends AppCompatActivity {

    private String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_trip);

        SharedPreferences preferences = getSharedPreferences(Constants.userDetailsSharedPreferences, Context.MODE_PRIVATE);
        token = preferences.getString(Constants.token, "");

        Window window = this.getWindow();
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimaryDark));
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.addToolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_white_24dp));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        toolbar.setTitle("Add Trip");
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorWhite));

        setUpAddTripFragment();

    }


    private void setUpAddTripFragment(){
        FragmentManager manager = getFragmentManager();
        AddTripFragment addTripFragment = new AddTripFragment();
        Bundle args = new Bundle();
        args.putString(Constants.token, token);
        addTripFragment.setArguments(args);
        manager.beginTransaction().replace(R.id.addActivityFrameLayout, addTripFragment).commit();
    }
}
