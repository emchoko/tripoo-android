package com.tripoo.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.tripoo.R;
import com.tripoo.rest.RestRetriever;
import com.tripoo.rest.RestService;
import com.tripoo.rest.model.get.UserName;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchUser extends AppCompatActivity {

    @BindView(R.id.usernameSearchEdtText)
    EditText searchEdtTxt;

    @BindView(R.id.searchBtn)
    Button searchBtn;

    @BindView(R.id.searchUsernameTextView)
    TextView userDetailsTxtView;

    @BindView(R.id.messageUserBtn)
    Button messageBtn;

    private UserName userName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_user);

        ButterKnife.bind(this);
        Window window = this.getWindow();
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.customToolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_white_24dp));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        toolbar.setTitle("Search User");
        toolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.colorWhite));

        initViews();
    }

    private void initViews() {
        searchEdtTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() > 0)
                    searchBtn.setEnabled(true);
                else
                    searchBtn.setEnabled(false);
            }
        });

        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                makeRestCall(searchEdtTxt.getText().toString());
            }
        });

        messageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.putExtra("username", userName.getUsername());
                intent.putExtra("id", userName.getId() + "");

                setResult(Activity.RESULT_OK, intent);
                finish();
            }
        });

        searchBtn.setEnabled(false);
        userDetailsTxtView.setVisibility(View.GONE);
        messageBtn.setVisibility(View.GONE);
    }

    private void setOnCallResultViews(String username) {
        userDetailsTxtView.setVisibility(View.VISIBLE);
        messageBtn.setVisibility(View.VISIBLE);
        userDetailsTxtView.setText(username);

    }

    private void makeRestCall(final String username) {
        RestService service = RestRetriever.getClient().create(RestService.class);
        Call<UserName> call = service.getUserByUsername("user_by_username", username);

        call.enqueue(new Callback<UserName>() {
            @Override
            public void onResponse(Call<UserName> call, Response<UserName> response) {
                setOnCallResultViews(response.body().getUsername());
                userName = response.body();
            }

            @Override
            public void onFailure(Call<UserName> call, Throwable t) {
                //TODO show error
            }
        });


    }
}
