package com.tripoo.activities;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.tripoo.MainActivity;
import com.tripoo.R;
import com.tripoo.rest.RestRetriever;
import com.tripoo.rest.RestService;
import com.tripoo.rest.model.get.UserName;
import com.tripoo.rest.model.post.request.UserAuth;
import com.tripoo.rest.model.post.response.Token;
import com.tripoo.utils.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.READ_CONTACTS;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity {

    /**
     * Id to identity READ_CONTACTS permission request.
     */
    private static final int REQUEST_READ_CONTACTS = 0;

    private static final String TAG = LoginActivity.class.getSimpleName();

    @BindView(R.id.email)
    public AutoCompleteTextView mUsernameView;
    @BindView(R.id.password)
    public EditText mPasswordView;

    @BindView(R.id.wrongCredentialsTextView)
    public TextView badCredentialsTextView;

    @BindView(R.id.signUpTextView)
    public TextView signUpTextView;


    ProgressDialog progress;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        SharedPreferences sharedPreferences = getSharedPreferences(Constants.userDetailsSharedPreferences,Context.MODE_PRIVATE);
        String username = sharedPreferences.getString(Constants.username, "");
        String pass = sharedPreferences.getString(Constants.password, "");


        mUsernameView.setText(username);
        mPasswordView.setText(pass);

        initViews();

    }

    @OnClick(R.id.signUpTextView)
    public void signUpClick(){
        startActivity(new Intent(LoginActivity.this, RegistrationActivity.class));
    }

    private void setUpSignUpSpan(){
        Spannable text = new SpannableString("Not a member? Sign up!");
        text.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorPrimary)), 14, 22, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        signUpTextView.setText(text);
    }

    private void initViews() {
        Window window = this.getWindow();
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimaryDark));
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.loginToolbar);
        toolbar.setTitle("Login");
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorWhite));
        setSupportActionBar(toolbar);

        setUpSignUpSpan();
        // Set up the login form.
        populateAutoComplete();

        badCredentialsTextView.setVisibility(View.GONE);

        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });
    }

    @OnClick(R.id.signInButton)
    public void onSignInClick(){
        attemptLogin();
    }

    private void populateAutoComplete() {
        if (!mayRequestContacts()) {
            return;
        }
    }

    private boolean mayRequestContacts() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (checkSelfPermission(READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (shouldShowRequestPermissionRationale(READ_CONTACTS)) {
            Snackbar.make(mUsernameView, "Permission needed", Snackbar.LENGTH_INDEFINITE)
                    .setAction(android.R.string.ok, new View.OnClickListener() {
                        @Override
                        @TargetApi(Build.VERSION_CODES.M)
                        public void onClick(View v) {
                            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
                        }
                    });
        } else {
            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
        }
        return false;
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_READ_CONTACTS) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                populateAutoComplete();
            }
        }
    }


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        // Reset errors.
        mUsernameView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String email = mUsernameView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError("Invalid password");
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mUsernameView.setError("Username required");
            focusView = mUsernameView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            callAuthentication(email, password);
        }
    }


    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }


    private void getUserIdByUsername(final String username, final String password, final String token) {
        RestService service = RestRetriever.getClient().create(RestService.class);
        Call<UserName> call = service.getUserByUsername("user_by_username", username);

        call.enqueue(new Callback<UserName>() {
            @Override
            public void onResponse(Call<UserName> call, Response<UserName> response) {
                progress.dismiss();
                switch (response.code()) {
                    case 200:

                        SharedPreferences sharedPref = LoginActivity.this.getSharedPreferences(Constants.userDetailsSharedPreferences, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPref.edit();
                        editor.putString(Constants.username, username);
                        editor.putString(Constants.password, password);
                        editor.putString(Constants.token, token);
                        editor.putLong(Constants.id, response.body().getId());
                        editor.apply();
                        editor.commit();


                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        startActivity(intent);
                        break;
                    default:
                        //TODO: ERROR
                        Toast.makeText(LoginActivity.this, "Something went wrong!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<UserName> call, Throwable t) {
                progress.dismiss();
                Toast.makeText(LoginActivity.this, "" + t.getCause(), Toast.LENGTH_SHORT).show();
                Log.e(TAG, t.getMessage());
            }
        });
    }

    private void callAuthentication(final String username, final String password) {
        progress = ProgressDialog.show(LoginActivity.this, "Authentication", "Checking credentials....", true, false);

        RestService service = RestRetriever.getClient().create(RestService.class);
        UserAuth userAuth = new UserAuth(username, password);
        Call<Token> call = service.authenticateUser(userAuth);

        call.enqueue(new Callback<Token>() {
            @Override
            public void onResponse(Call<Token> call, Response<Token> response) {
                switch (response.code()) {
                    case 200:
                        getUserIdByUsername(username, password, response.body().getToken());
                        break;
                    case 401:
                        Toast.makeText(LoginActivity.this, "Wrong credentials: " + response.message(), Toast.LENGTH_SHORT).show();
                        mUsernameView.setError("");
                        mPasswordView.setError("");
                        badCredentialsTextView.setVisibility(View.VISIBLE);
                        break;
                }
            }

            @Override
            public void onFailure(Call<Token> call, Throwable t) {
                progress.dismiss();
                Toast.makeText(LoginActivity.this, "" + t.getCause(), Toast.LENGTH_SHORT).show();
                Log.e(TAG, t.getMessage());
            }
        });
    }
}

