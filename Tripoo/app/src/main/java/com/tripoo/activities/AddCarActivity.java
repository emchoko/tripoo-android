package com.tripoo.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.tripoo.R;
import com.tripoo.fragment.main.AddCarFragment;

public class AddCarActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_car);

        Toolbar toolbar = (Toolbar) findViewById(R.id.carToolbar);
        toolbar.setTitle("Create Car");
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_white_24dp));


        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        Window window = this.getWindow();
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimaryDark));
        setUpAddCarFragment();
    }

    private void setUpAddCarFragment() {
        android.app.FragmentManager manager = getFragmentManager();
        AddCarFragment fragment = new AddCarFragment();
        manager.beginTransaction().replace(R.id.carActivityFrameLayout, fragment).commit();
    }
}
