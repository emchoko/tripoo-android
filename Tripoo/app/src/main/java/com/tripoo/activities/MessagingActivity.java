package com.tripoo.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.sendbird.android.BaseChannel;
import com.sendbird.android.BaseMessage;
import com.sendbird.android.GroupChannel;
import com.sendbird.android.GroupChannelListQuery;
import com.sendbird.android.SendBird;
import com.sendbird.android.SendBirdException;
import com.sendbird.android.User;
import com.tripoo.R;
import com.tripoo.messaging.adapter.GroupsAdapter;
import com.tripoo.services.InstanceIdService;
import com.tripoo.services.SendBirdMessagingService;
import com.tripoo.utils.Constants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MessagingActivity extends AppCompatActivity {

    private static final String HANDLER_ID = "121212";
    private static final String TAG = MessagingActivity.class.getSimpleName();
    private static final int REQUEST_CODE = 258;

    @BindView(R.id.groupsRecyclerView)
    RecyclerView groupsRecyclerView;


    private GroupsAdapter adapter;
    private String username;
    private String userId;
    private ProgressDialog progressDialog;
    private Map<String, String> channelMembersDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messaging);

        startService(new Intent(this, InstanceIdService.class));
        startService(new Intent(this, SendBirdMessagingService.class));

        ButterKnife.bind(this);

        channelMembersDetails = new HashMap<>();

        SharedPreferences sharedPreferences = getSharedPreferences(Constants.userDetailsSharedPreferences, Context.MODE_PRIVATE);
        username = sharedPreferences.getString(Constants.username, "");
        userId = sharedPreferences.getLong(Constants.id, 0) + "";

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MessagingActivity.this, SearchUser.class);
                startActivityForResult(intent, REQUEST_CODE);
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        setGroupsRecyclerView(new ArrayList<GroupChannel>());
        sendBirdStart(userId);
        progressDialog = ProgressDialog.show(MessagingActivity.this, "Loading messages", "Connecting to messaging service ...", true, false);


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CODE:
                if (resultCode == Activity.RESULT_OK) {
                    List<String> userIds = new ArrayList<>();
                    String username = data.getExtras().getString("username");
                    String id = data.getExtras().getString("id");
                    userIds.add(id);
                    userIds.add(userId);
                    createGroupChannel(userIds);
                }
                break;
        }
    }

    private void sendBirdStart(String userId) {
        initSendBird();
        connectSendBird(userId, username);
        setChannelHandler();
    }

    private void setChannelHandler() {
        SendBird.addChannelHandler(HANDLER_ID, new SendBird.ChannelHandler() {
            @Override
            public void onMessageReceived(BaseChannel baseChannel, BaseMessage baseMessage) {
                Toast.makeText(MessagingActivity.this, "Received message: " + baseMessage.getMessageId(), Toast.LENGTH_SHORT).show();
                Log.d(TAG, "Received message: " + baseMessage.getMessageId());
            }
        });
    }

    private void getChannelListQuery() {

        final GroupChannelListQuery channelListQuery = GroupChannel.createMyGroupChannelListQuery();
        channelListQuery.next(new GroupChannelListQuery.GroupChannelListQueryResultHandler() {
            @Override
            public void onResult(List<GroupChannel> list, SendBirdException e) {
                progressDialog.dismiss();
                if (e != null) {
                    Log.d(TAG, e.getMessage());
                    return;
                }

                while (channelListQuery.isLoading()) ;
                adapter.addAll(list);
                adapter.notifyDataSetChanged();

                if(list != null && list.size() > 0)
                for (GroupChannel groupChannel : list) {
                    for (User user : groupChannel.getMembers()) {
                        channelMembersDetails.put(user.getNickname(), user.getUserId());
                    }
                }


                Toast.makeText(MessagingActivity.this, "List count:" + list.size(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setGroupsRecyclerView(List<GroupChannel> list) {
        groupsRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new GroupsAdapter(list, R.layout.group_chats_list_row, this, userId);
        groupsRecyclerView.setAdapter(adapter);
    }

    private void createGroupChannel(List<String> userIds) {
        GroupChannel.createChannelWithUserIds(userIds, true, new GroupChannel.GroupChannelCreateHandler() {
            @Override
            public void onResult(GroupChannel groupChannel, SendBirdException e) {
                if (e != null) {
                    Log.e(TAG, e.getMessage());
                    return;
                }
                adapter.add(groupChannel);
                adapter.notifyDataSetChanged();
            }
        });
    }


    private void initSendBird() {
        SendBird.init(getString(R.string.send_bird_app_id), this);
        Log.d(TAG, "Initializing Send Bird");
    }

    private void connectSendBird(String userId, final String username) {
        SendBird.connect(userId, username, new SendBird.ConnectHandler() {
            @Override
            public void onConnected(User user, SendBirdException e) {
                if (e != null) {
                    Toast.makeText(MessagingActivity.this, "An Error Occured With Messaging", Toast.LENGTH_SHORT).show();
                    return;
                }


                SendBird.updateCurrentUserInfo(username, null, new SendBird.UserInfoUpdateHandler() {
                    @Override
                    public void onUpdated(SendBirdException e) {
                        progressDialog.dismiss();
                        progressDialog = ProgressDialog.show(MessagingActivity.this, "Loading chats", "Fetching chat rooms ...", true, false);
                        getChannelListQuery();
                    }
                });


                if (FirebaseInstanceId.getInstance().getToken() == null) return;

                SendBird.registerPushTokenForCurrentUser(FirebaseInstanceId.getInstance().getToken(), new SendBird.RegisterPushTokenWithStatusHandler() {
                    @Override
                    public void onRegistered(SendBird.PushTokenRegistrationStatus pushTokenRegistrationStatus, SendBirdException e) {
                        if (e != null) {
                            Log.d(TAG, e.getMessage());
                            return;
                        }
                        Log.d(TAG, "Registration of token: " + FirebaseInstanceId.getInstance().getToken());
                    }
                });
            }
        });
    }

    @Override
    protected void onDestroy() {
        SendBird.disconnect(new SendBird.DisconnectHandler() {
            @Override
            public void onDisconnected() {
                Toast.makeText(MessagingActivity.this, "Disconnected from messaging", Toast.LENGTH_SHORT).show();
            }
        });
        super.onDestroy();
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();

    }
}
