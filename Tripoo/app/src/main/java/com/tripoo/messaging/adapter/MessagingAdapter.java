package com.tripoo.messaging.adapter;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sendbird.android.BaseMessage;
import com.sendbird.android.GroupChannel;
import com.sendbird.android.SendBird;
import com.sendbird.android.User;
import com.sendbird.android.UserMessage;
import com.squareup.picasso.Picasso;
import com.tripoo.R;
import com.tripoo.utils.Helper;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by emil on 1/22/17.
 */
public class MessagingAdapter extends BaseAdapter {
    private static final int USER_MESSAGE = 1;
    private static final int UNSUPPORTED = 2;

    private Context context;
    private LayoutInflater inflater;
    private List<Object> itemList;
    private GroupChannel groupChannel;

    public MessagingAdapter(Context context, LayoutInflater inflater, List<Object> itemList, GroupChannel groupChannel) {
        this.context = context;
        this.inflater = inflater;
        this.itemList = itemList;
        this.groupChannel = groupChannel;
    }

    public MessagingAdapter(Context context, GroupChannel channel) {
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        itemList = new ArrayList<>();
        this.groupChannel = channel;
    }

    @Override
    public int getCount() {
        return itemList.size() + (groupChannel.isTyping() ? 1 : 0);
    }

    @Override
    public Object getItem(int position) {
        if (position >= itemList.size()) {
            List<User> members = groupChannel.getTypingMembers();
            ArrayList<String> names = new ArrayList<>();
            for (User member : members) {
                names.add(member.getNickname());
            }
        }

        return itemList.get(position);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder holder;
        Object item = getItem(position);
        if (view != null) {
            holder = (ViewHolder) view.getTag();
        } else {
            view = inflater.inflate(R.layout.chat_messages_layout, parent, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        }

        //Doing the message recognition here
        switch (getItemViewType(position)) {
            case USER_MESSAGE:
                UserMessage message = (UserMessage) item;
                if (message.getSender().getUserId().equals(SendBird.getCurrentUser().getUserId())) {
                    //Disable other user
                    holder.otherLinearLayout.setVisibility(View.GONE);
//                    holder.otherProfilePicture.setVisibility(View.GONE);
//                    holder.otherUsernameTextView.setVisibility(View.GONE);
//                    holder.otherUserMessage.setVisibility(View.GONE);
//                    holder.otherUserTimeStamp.setVisibility(View.GONE);

                    //Enable mine
                    holder.mineLinearLayout.setVisibility(View.VISIBLE);
                    Picasso.with(context).load(message.getSender().getProfileUrl()).into(holder.mineProfilePicture);
                    holder.mineMessage.setText(message.getMessage());
                    holder.mineTimeStamp.setText(Helper.getDisplayDateTime(context, message.getCreatedAt()));
                } else {
                    holder.mineLinearLayout.setVisibility(View.GONE);
                    holder.otherLinearLayout.setVisibility(View.VISIBLE);

                    holder.otherUserTimeStamp.setText(Helper.getDisplayDateTime(context, message.getCreatedAt()));
                    holder.otherUserMessage.setText(message.getMessage());
                    Picasso.with(context).load(message.getSender().getProfileUrl()).into(holder.otherProfilePicture);
                    holder.otherUsernameTextView.setText(message.getSender().getNickname());

                }
                break;
            case UNSUPPORTED:
                break;
        }

        return view;
    }

    public void delete(Object obj) {
        itemList.remove(obj);
    }

    public void clear() {
        itemList.clear();
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        Object item = itemList.get(position);
        if (item instanceof UserMessage)
            return USER_MESSAGE;
        return UNSUPPORTED;
    }

    public void insertMessage(BaseMessage message) {
        itemList.add(0, message);
    }

    public void appendMessage(BaseMessage message) {
        itemList.add(message);
    }

    static class ViewHolder {
        //Other user views
        @BindView(R.id.otherUserMessageLinearLayout)
        LinearLayout otherLinearLayout;
        @BindView(R.id.otherUserImageView)
        ImageView otherProfilePicture;
        @BindView(R.id.otherUsernameTextView)
        TextView otherUsernameTextView;
        @BindView(R.id.otherUserMessage)
        TextView otherUserMessage;
        @BindView(R.id.otherUserMessageTimeStampTextView)
        TextView otherUserTimeStamp;

        //Mine views
        @BindView(R.id.mineLinearLayout)
        LinearLayout mineLinearLayout;
        @BindView(R.id.mineProfilePhoto)
        ImageView mineProfilePicture;
        @BindView(R.id.mineMessageTextView)
        TextView mineMessage;
        @BindView(R.id.mineMessageTimeStampTextView)
        TextView mineTimeStamp;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

}