package com.tripoo.messaging.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sendbird.android.AdminMessage;
import com.sendbird.android.BaseMessage;
import com.sendbird.android.FileMessage;
import com.sendbird.android.GroupChannel;
import com.sendbird.android.UserMessage;
import com.squareup.picasso.Picasso;
import com.tripoo.R;
import com.tripoo.activities.ChatRoomActivity;
import com.tripoo.utils.Helper;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by emil on 1/21/17.
 */

public class GroupsAdapter extends RecyclerView.Adapter<GroupsAdapter.GroupViewHolder> {

    private static final String TAG = GroupsAdapter.class.getSimpleName();
    private List<GroupChannel> list;
    private int rowLayout;
    private Context context;
    private String myId;


    public GroupsAdapter(List<GroupChannel> list, int rowLayout, Context context, String myId) {
        this.list = list;
        this.rowLayout = rowLayout;
        this.context = context;
        this.myId = myId;
    }

    @Override
    public GroupViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new GroupViewHolder(view);
    }

    @Override
    public void onBindViewHolder(GroupViewHolder holder, int position) {
        final GroupChannel channel = list.get(position);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ChatRoomActivity.class);
                intent.putExtra(context.getString(R.string.channel_url), channel.getUrl());
                intent.putExtra("channel_name", (Helper.convertMembersToString(channel.getMembers(), myId)));
                context.startActivity(intent);
            }
        });

        Log.d(TAG, channel.getCoverUrl());
        Picasso.with(context).load(channel.getCoverUrl()).into(holder.groupImage);
        holder.groupTopic.setText(Helper.convertMembersToString(channel.getMembers(), myId));
        BaseMessage message = channel.getLastMessage();
//        holder.groupDate.setText(Helper.getDisplayDateTime(context, message.getCreatedAt()));
        holder.groupLastMessage.setText(messageTypeConverter(message));
        holder.groupUnreadCountTxtView.setText(channel.getUnreadMessageCount() + "");
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void addAll(List<GroupChannel> newList) {
        list.addAll(newList);
    }

    public void add(GroupChannel groupChannel) {
        list.add(groupChannel);
    }

    public static class GroupViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.groupImage)
        ImageView groupImage;
        @BindView(R.id.groupTopicTxtView)
        TextView groupTopic;
        @BindView(R.id.groupDateTxtView)
        TextView groupDate;
        @BindView(R.id.groupLastMessage)
        TextView groupLastMessage;
        @BindView(R.id.groupUnreadCountTxtView)
        TextView groupUnreadCountTxtView;

        public GroupViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    private String messageTypeConverter(BaseMessage message) {
        if (message instanceof UserMessage) {
            return ((UserMessage) message).getMessage();
        } else if (message instanceof AdminMessage) {
            return ((AdminMessage) message).getMessage();
        } else if (message instanceof FileMessage) {
            return "File";
        }
        return null;
    }


}
