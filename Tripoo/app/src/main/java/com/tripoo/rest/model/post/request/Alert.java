package com.tripoo.rest.model.post.request;

/**
 * Created by emil on 2/14/17.
 */

public class Alert {

    private String startPoint;
    private String endPoint;
    private Long date;

    public Alert(String startPoint, String endPoint, Long date) {
        this.startPoint = startPoint;
        this.endPoint = endPoint;
        this.date = date;
    }

    public String getStartPoint() {
        return startPoint;
    }

    public void setStartPoint(String startPoint) {
        this.startPoint = startPoint;
    }

    public String getEndPoint() {
        return endPoint;
    }

    public void setEndPoint(String endPoint) {
        this.endPoint = endPoint;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }
}
