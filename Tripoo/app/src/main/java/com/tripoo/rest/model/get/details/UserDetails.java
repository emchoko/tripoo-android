package com.tripoo.rest.model.get.details;

/**
 * Created by emil on 1/9/17.
 */

public class UserDetails {

    private Integer id;
    private String username;
    private String firstName;
    private String lastName;
    private Object birthDate;
    private Integer rating;
    private Object phoneNumber;
    private Object linkToFacebook;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Object getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Object birthDate) {
        this.birthDate = birthDate;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public Object getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(Object phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Object getLinkToFacebook() {
        return linkToFacebook;
    }

    public void setLinkToFacebook(Object linkToFacebook) {
        this.linkToFacebook = linkToFacebook;
    }

}
