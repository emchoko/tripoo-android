package com.tripoo.rest.model.get.details;

import com.tripoo.rest.model.post.request.CarDetailsNoId;

/**
 * Created by emil on 1/9/17.
 */

public class CarDetails extends CarDetailsNoId {

    private Long id;

    public CarDetails(String manufacturer, String model, int year, int comfortLevel, Long id) {
        super(manufacturer, model, year, comfortLevel);
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
