package com.tripoo.rest.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.tripoo.R;
import com.tripoo.rest.RestRetriever;
import com.tripoo.rest.RestService;
import com.tripoo.rest.model.Review;
import com.tripoo.rest.model.get.list.BookingList;
import com.tripoo.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by emil on 2/15/17.
 */

public class BookingAdapter extends RecyclerView.Adapter<BookingAdapter.BookingViewHolder> {

    private static final String TAG = RouteAdapter.class.getSimpleName();

    private List<BookingList> bookings;
    private int rowLayout;
    private Context context;
    private boolean isBooked;

    public BookingAdapter(List<BookingList> bookings, int rowLayout, Context context) {
        this.bookings = bookings;
        this.rowLayout = rowLayout;
        this.context = context;


        for (int i = 1; i <= 5; i++) {
            rate.add(i);
        }
    }

    @Override
    public BookingViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new BookingViewHolder(view);
    }

    private String comment = "";
    int rating = 1;
    List<Integer> rate = new ArrayList<>();



    @Override
    public void onBindViewHolder(BookingViewHolder holder, final int position) {
        holder.startPoint.setText(bookings.get(position).getRoute().getStartPoint());
        holder.endPoint.setText(bookings.get(position).getRoute().getEndPoint());
        holder.bookBtn.setText("ADD REVIEW");


        holder.bookBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new MaterialDialog.Builder(context)
                        .title("Review")
                        .inputRangeRes(10, 200, R.color.colorPrimary)
                        .input(null, null, new MaterialDialog.InputCallback() {
                            @Override
                            public void onInput(MaterialDialog dialog, CharSequence input) {
                                comment = input.toString();
                            }
                        })
                        .positiveText("ADD")
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                new MaterialDialog.Builder(context)
                                        .items(rate)
                                        .itemsCallbackSingleChoice(0, new MaterialDialog.ListCallbackSingleChoice() {
                                            @Override
                                            public boolean onSelection(MaterialDialog dialog, View itemView, int which, CharSequence text) {
                                                rating = Integer.parseInt(text.toString());
                                                return true;
                                            }
                                        })
                                        .positiveText("ADD")
                                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                                            @Override
                                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                                Log.d(TAG, "Rating: " + rating + "\nReview: " + comment);
                                                makeReview(comment, rating, bookings.get(position).getId());
                                            }
                                        })
                                        .show();
                            }
                        }).show();
            }
        });
    }

    private void makeReview(String comment, int rating, Integer id) {
        Review review = new Review();
        review.setDescription(comment);
        review.setRating(rating);

        SharedPreferences prefs = context.getSharedPreferences(Constants.userDetailsSharedPreferences, Context.MODE_PRIVATE);
        String token = prefs.getString(Constants.token, "");


        RestService service = RestRetriever.getClient().create(RestService.class);
        Call<Void> call = service.makeReview(token, review, Long.valueOf(id));

        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Toast.makeText(context.getApplicationContext(), "Made review", Toast.LENGTH_SHORT).show();
                Log.d(TAG, "Made review " + response.code());
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.d(TAG, "Failure review ");

            }
        });
    }

    @Override
    public int getItemCount() {
        return bookings.size();
    }

    public static class BookingViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.cardStartPoint)
        TextView startPoint;
        @BindView(R.id.cardEndPoint)
        TextView endPoint;
        @BindView(R.id.cardBookTripBtn)
        Button bookBtn;

        public BookingViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


}
