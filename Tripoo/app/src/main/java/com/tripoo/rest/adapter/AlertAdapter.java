package com.tripoo.rest.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tripoo.R;
import com.tripoo.rest.model.get.details.CarDetails;
import com.tripoo.rest.model.post.request.Alert;
import com.tripoo.utils.NumberToMonthConverter;

import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by emil on 2/14/17.
 */

public class AlertAdapter extends RecyclerView.Adapter<AlertAdapter.AlertViewHolder> {

    private int rowLayout;
    private List<Alert> alerts;
    private Context context;

    public AlertAdapter(int rowLayout, List<Alert> alerts, Context context) {
        this.rowLayout = rowLayout;
        this.alerts = alerts;
        this.context = context;
    }

    @Override
    public AlertViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new AlertViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AlertViewHolder holder, int position) {
        holder.startPoint.setText(alerts.get(position).getStartPoint());
        holder.endPoint.setText(alerts.get(position).getEndPoint());
        holder.date.setText(NumberToMonthConverter.formatDate(new Date(alerts.get(position).getDate())));
    }

    @Override
    public int getItemCount() {
        return alerts.size();
    }

    public class AlertViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.startPointTxtView)
        TextView startPoint;
        @BindView(R.id.endPointTxtView)
        TextView endPoint;
        @BindView(R.id.dateTxtView)
        TextView date;

        public AlertViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
