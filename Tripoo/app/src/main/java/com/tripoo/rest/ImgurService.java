package com.tripoo.rest;

import com.tripoo.rest.model.post.response.ImageResponse;

import java.util.Map;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PartMap;

/**
 * Created by emil on 1/29/17.
 */

public interface ImgurService {
    String BASE_URL = "https://api.imgur.com";

    @Multipart
    @POST("/3/image")
    Call<ImageResponse> postImage(
            @Header("Authorization") String auth,
            @PartMap Map<String, RequestBody> image);
}
