package com.tripoo.rest;

import android.util.Log;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by emil on 1/6/17.
 */

public class RestRetriever {

    private static final String TAG = RestRetriever.class.getSimpleName();

    private static final String BASE_URL = "http://176.12.32.210:5000/";

    private static Retrofit retrofit;

    public static Retrofit getClient() {
        Log.d(TAG, "URL: " + BASE_URL);
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

    public static Retrofit getImageAdapter(){
        Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(ImgurService.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

        return retrofit;
    }

}
