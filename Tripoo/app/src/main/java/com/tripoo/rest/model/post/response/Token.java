package com.tripoo.rest.model.post.response;

/**
 * Created by emil on 1/14/17.
 */

public class Token {
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
