package com.tripoo.rest.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tripoo.R;
import com.tripoo.rest.model.get.details.RouteDetails;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by emil on 1/13/17.
 */
public class EmptyAdapter extends RecyclerView.Adapter<EmptyAdapter.EmptyViewHolder> {

    private static final String TAG = EmptyAdapter.class.getSimpleName();
    private int rowLayout;
    private String message;


    public EmptyAdapter(String message, int rowLayout) {
        this.rowLayout = rowLayout;
        this.message = message;
    }

    @Override
    public EmptyAdapter.EmptyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new EmptyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(EmptyViewHolder holder, int position) {
        holder.textView.setText(message);
    }


    @Override
    public int getItemCount() {
        return 1;
    }

    public static class EmptyViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.emptyTextView)
        TextView textView;
        public EmptyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
