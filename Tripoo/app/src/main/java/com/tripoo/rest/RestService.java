package com.tripoo.rest;

import com.tripoo.rest.adapter.RouteAdapter;
import com.tripoo.rest.model.Booking;
import com.tripoo.rest.model.Review;
import com.tripoo.rest.model.get.UserName;
import com.tripoo.rest.model.get.details.CarDetails;
import com.tripoo.rest.model.get.details.RouteDetails;
import com.tripoo.rest.model.get.details.TripDetails;
import com.tripoo.rest.model.get.list.BookingList;
import com.tripoo.rest.model.get.list.RouteList;
import com.tripoo.rest.model.get.list.TripList;
import com.tripoo.rest.model.get.list.UserList;
import com.tripoo.rest.model.get.profile.UserProfile;
import com.tripoo.rest.model.post.request.Alert;
import com.tripoo.rest.model.post.request.CarDetailsNoId;
import com.tripoo.rest.model.post.request.Route;
import com.tripoo.rest.model.post.request.Trip;
import com.tripoo.rest.model.post.request.UserAuth;
import com.tripoo.rest.model.post.request.UserRegistration;
import com.tripoo.rest.model.post.response.Token;
import com.tripoo.services.InstanceIdService;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by emil on 1/7/17.
 */

public interface RestService {
    @GET("users/")
    Call<List<UserList>> getUserList(@Query("mapping") String mapping);

    @GET("trips/")
    Call<List<TripList>> getTripList(@Query("mapping") String mapping,
                                     @Query("startPoint") String startPoint,
                                     @Query("endPoint") String endPoint,
                                     @Query("date") Long date);

    @GET("trips/{id}")
    Call<TripDetails> getTripById(@Path("id") Long tripId, @Query("mapping") String mapping);

    @GET("users/{id}")
    Call<UserProfile> getUserById(@Path("id") Long userId, @Query("mapping") String mapping);

    @GET("users")
    Call<UserName> getUserByUsername(@Query("mapping") String mapping, @Query("username") String username);

    @GET("users/{id}/bookings")
    Call<List<BookingList>> getUserBookingsById(@Path("id") Long userId, @Query("mapping") String mapping);

    @GET("users/{id}/trips")
    Call<List<TripList>> getTripByUserId(@Path("id") Long userId, @Query("mapping") String mapping);

    @GET("car/{id}")
    Call<CarDetails> getCarById(@Path("id") Long carId, @Query("mapping") String mapping);

    @GET("users/{id}/cars")
    Call<List<CarDetails>> getCarsByUserId(@Path("id") Long userId, @Query("mapping") String mapping);

    @GET("users/{id}/alerts")
    Call<List<Alert>> getAlertsByUserId(@Path("id") Long userId, @Query("mapping") String mapping);

    @POST("cars")
    Call<CarDetails> createCar(@Body CarDetailsNoId carDetails, @Header("Authorization") String token);

    @POST("auth")
    Call<Token> authenticateUser(@Body UserAuth auth);

    @POST("trips")
    Call<Trip> addTrip(@Body Trip trip, @Header("Authorization") String token);

    @POST("trips/{id}/routes")
    Call<Route> addRouteToTripId(@Path("id") Long tripId, @Body Route route, @Header("Authorization") String token);


    @POST("users")
    Call<UserRegistration> registrationOfUser(@Body UserRegistration userRegistration);

    @PATCH("users/{id}/photoUrl")
    Call<String> patchPhotoUrl(@Header("Authorization") String token, @Path("id") Long userId, @Body UploadService.UserPhoto photo);

    @PATCH("users/{id}/token")
    Call<String> patchToken(@Header("Authorization") String token, @Path("id") Long userId, @Body Token body);

    @POST("alerts")
    Call<Void> createAlert(@Header("Authorization") String token, @Body Alert alert);

    @POST("routes/{id}/book")
    Call<Void> bookRouteById(@Header("Authorization") String token, @Body Booking booking, @Path("id") Long id);

    @POST("bookings/{id}/reviews")
    Call<Void> makeReview(@Header("Authorization") String token, @Body Review review, @Path("id") Long id);
}
