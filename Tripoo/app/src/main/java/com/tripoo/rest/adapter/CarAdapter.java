package com.tripoo.rest.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.tripoo.R;
import com.tripoo.rest.model.get.details.CarDetails;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by emil on 1/17/17.
 */

public class CarAdapter extends RecyclerView.Adapter<CarAdapter.CarViewHolder> {
    private int rowLayout;
    private List<CarDetails> cars;
    private Context context;


    public CarAdapter(List<CarDetails> cars, int rowLayout, Context context) {
        this.cars = cars;
        this.rowLayout = rowLayout;
        this.context = context;
    }

    @Override
    public CarViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new CarViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CarViewHolder holder, int position) {
        Picasso.with(context).load(cars.get(position).getPhotoUrl()).into(holder.carImageView);
        holder.carMakeTxtView.setText(cars.get(position).getManufacturer());
        holder.carModelTxtView.setText(cars.get(position).getModel());
        holder.carYearTxtView.setText(cars.get(position).getYear() + "");
        holder.carComfortTxtView.setText(cars.get(position).getComfortLevel() + "");
    }

    @Override
    public int getItemCount() {
        return cars.size();
    }

    public static class CarViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.carMake)
        TextView carMakeTxtView;
        @BindView(R.id.carModel)
        TextView carModelTxtView;
        @BindView(R.id.carYear)
        TextView carYearTxtView;
        @BindView(R.id.carComfort)
        TextView carComfortTxtView;
        @BindView(R.id.carImageView)
        ImageView carImageView;

        public CarViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
