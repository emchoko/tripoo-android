package com.tripoo.rest.model.get.details;

import java.util.List;

/**
 * Created by emil on 1/9/17.
 */
public class TripDetails {

    private List<RouteDetails> routes = null;
    private String luggage;
    private String tripComment;
    private CarDetails car;
    private UserDetails user;
    private String date;
    private Integer price;

    public List<RouteDetails> getRoutes() {
        return routes;
    }

    public void setRoutes(List<RouteDetails> routes) {
        this.routes = routes;
    }

    public String getLuggage() {
        return luggage;
    }

    public void setLuggage(String luggage) {
        this.luggage = luggage;
    }

    public String getTripComment() {
        return tripComment;
    }

    public void setTripComment(String tripComment) {
        this.tripComment = tripComment;
    }

    public CarDetails getCar() {
        return car;
    }

    public void setCar(CarDetails car) {
        this.car = car;
    }

    public UserDetails getUser() {
        return user;
    }

    public void setUser(UserDetails user) {
        this.user = user;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

}