package com.tripoo.rest.model.get.list;

/**
 * Created by emil on 2/15/17.
 */

public class BookingList {

    private Integer id;
    private Integer userId;
    private RouteList route;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public RouteList getRoute() {
        return route;
    }

    public void setRoute(RouteList route) {
        this.route = route;
    }

}
