package com.tripoo.rest.model.post.request;

import java.io.Serializable;
import java.util.List;

/**
 * Created by emil on 1/14/17.
 */

public class Trip implements Serializable {

    private Long date;
    private Integer price;
    private String luggage;
    private String tripComment;
    private List<Route> routes;

    public Trip() {
    }

    public Trip(Long date, Integer price, String luggage, String tripComment) {
        this.date = date;
        this.price = price;
        this.luggage = luggage;
        this.tripComment = tripComment;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getLuggage() {
        return luggage;
    }

    public void setLuggage(String luggage) {
        this.luggage = luggage;
    }

    public String getTripComment() {
        return tripComment;
    }

    public void setTripComment(String tripComment) {
        this.tripComment = tripComment;
    }

    @Override
    public String toString() {
        return "Trip{" +
                "date=" + date +
                ", price=" + price +
                ", luggage='" + luggage + '\'' +
                ", tripComment='" + tripComment + '\'' +
                ", routes=" + routes +
                '}';
    }

    public List<Route> getRoutes() {
        return routes;
    }

    public void setRoutes(List<Route> routes) {
        this.routes = routes;
    }
}

