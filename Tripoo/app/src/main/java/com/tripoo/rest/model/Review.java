package com.tripoo.rest.model;

/**
 * Created by emil on 2/15/17.
 */
public class Review {

    private String description;
    private int rating;

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
