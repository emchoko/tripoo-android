package com.tripoo.rest.model.post.request;

import android.net.Uri;

import java.io.File;

/**
 * Created by emil on 1/29/17.
 */

public class UploadImage {

    public File image;
    public String title;
    public String description;
    public String albumId;
    public Uri uri;

}
