package com.tripoo.rest.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.tripoo.R;
import com.tripoo.activities.TripDetailsActivity;
import com.tripoo.rest.model.get.list.RouteList;
import com.tripoo.rest.model.get.list.TripList;
import com.tripoo.utils.Constants;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by emil on 1/8/17.
 */

public class TripAdapter extends RecyclerView.Adapter<TripAdapter.TripViewHolder> {

    private static final String TAG = TripAdapter.class.getSimpleName();

    private List<TripList> trips;
    private int rowLayout;
    private Context context;

    public static class TripViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.trip_layout)
        LinearLayout tripLayout;
        @BindView(R.id.cardTripDate)
        TextView date;
        @BindView(R.id.cardRoutes)
        TextView routes;
        @BindView(R.id.cardTripPrice)
        TextView price;
        @BindView(R.id.cardUserProfilePhoto)
        ImageView profilePicture;
        @BindView(R.id.cardUserNames)
        TextView userNames;
        @BindView(R.id.cardUserRating)
        TextView userRating;

        public TripViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public TripAdapter(List<TripList> trips, int rowLayout, Context context) {
        this.trips = trips;
        this.rowLayout = rowLayout;
        this.context = context;
    }

    @Override
    public TripViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new TripViewHolder(view);
    }


    @Override
    public void onBindViewHolder(TripViewHolder holder, final int position) {
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, TripDetailsActivity.class);
                intent.putExtra(Constants.trip_id, trips.get(position).getId());
                context.startActivity(intent);
            }
        });
        holder.date.setText(trips.get(position).getDate().toString());
        holder.routes.setText(convertRouteListToString(trips.get(position).getRoutes()));
        holder.price.setText(trips.get(position).getPrice()+"");
        Picasso.with(context).load(trips.get(position).getUser().getProfileUrl()).into(holder.profilePicture);
        Log.d(TAG, trips.get(position).getUser().toString());

        holder.userNames.setText(trips.get(position).getUser().getFirstName() + " " + trips.get(position).getUser().getLastName());
        holder.userRating.setText(trips.get(position).getUser().getRating()+"");
    }


    @Override
    public int getItemCount() {
        return trips.size();
    }

    private String convertRouteListToString(List<RouteList> list) {
        String result = "";
        for (RouteList route : list) {
            result += " " + route.getStartPoint() + " - " + route.getEndPoint() + " ";
        }
        return result;
    }


}
