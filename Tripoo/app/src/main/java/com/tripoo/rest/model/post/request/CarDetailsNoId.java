package com.tripoo.rest.model.post.request;

/**
 * Created by emil on 1/28/17.
 */

public class CarDetailsNoId {
    private String manufacturer;
    private String model;
    private int year;
    private int comfortLevel;
    private String photoUrl;

    public CarDetailsNoId(){}

    public CarDetailsNoId(String manufacturer, String model, int year, int comfortLevel) {
        this.manufacturer = manufacturer;
        this.model = model;
        this.year = year;
        this.comfortLevel = comfortLevel;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getComfortLevel() {
        return comfortLevel;
    }

    public void setComfortLevel(int comfortLevel) {
        this.comfortLevel = comfortLevel;
    }

    @Override
    public String toString() {
        return "CarDetails{" +
                "manufacturer='" + manufacturer + '\'' +
                ", model='" + model + '\'' +
                ", year=" + year +
                ", comfortLevel=" + comfortLevel +
                '}';
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }
}
