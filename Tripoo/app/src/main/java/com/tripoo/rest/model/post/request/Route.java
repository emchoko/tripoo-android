package com.tripoo.rest.model.post.request;

/**
 * Created by emil on 1/14/17.
 */
public class Route {

    private String startPoint;
    private String endPoint;
    private double startLatitude;
    private double startLongitude;
    private double endLatitude;
    private double endLongitude;
    private Integer totalFreePlaces;
    private Integer currentFreePlaces;
    private Long startTime;
    private Long estimatedEndTime;
    private Integer price;

    public Route() {
    }

    public Route(String startPoint,
                 String endPoint,
                 Double startLatitude,
                 Double startLongitude,
                 Double endLatitude,
                 Double endLongitude,
                 Integer totalFreePlaces,
                 Integer currentFreePlaces,
                 Long startTime,
                 Long estimatedEndTime) {

        this.startPoint = startPoint;
        this.endPoint = endPoint;
        this.startLatitude = startLatitude;
        this.startLongitude = startLongitude;
        this.endLatitude = endLatitude;
        this.endLongitude = endLongitude;
        this.totalFreePlaces = totalFreePlaces;
        this.currentFreePlaces = currentFreePlaces;
        this.startTime = startTime;
        this.estimatedEndTime = estimatedEndTime;
    }

    public String getStartPoint() {
        return startPoint;
    }

    public void setStartPoint(String startPoint) {
        this.startPoint = startPoint;
    }

    public String getEndPoint() {
        return endPoint;
    }

    public void setEndPoint(String endPoint) {
        this.endPoint = endPoint;
    }

    public double getStartLatitude() {
        return startLatitude;
    }

    public void setStartLatitude(double startLatitude) {
        this.startLatitude = startLatitude;
    }

    public double getStartLongitude() {
        return startLongitude;
    }

    public void setStartLongitude(double startLongitude) {
        this.startLongitude = startLongitude;
    }

    public double getEndLatitude() {
        return endLatitude;
    }

    public void setEndLatitude(double endLatitude) {
        this.endLatitude = endLatitude;
    }

    public double getEndLongitude() {
        return endLongitude;
    }

    public void setEndLongitude(double endLongitude) {
        this.endLongitude = endLongitude;
    }

    public Integer getTotalFreePlaces() {
        return totalFreePlaces;
    }

    public void setTotalFreePlaces(Integer totalFreePlaces) {
        this.totalFreePlaces = totalFreePlaces;
    }

    public Integer getCurrentFreePlaces() {
        return currentFreePlaces;
    }

    public void setCurrentFreePlaces(Integer currentFreePlaces) {
        this.currentFreePlaces = currentFreePlaces;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEstimatedEndTime() {
        return estimatedEndTime;
    }

    public void setEstimatedEndTime(Long estimatedEndTime) {
        this.estimatedEndTime = estimatedEndTime;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }


    @Override
    public String toString() {
        return "Route{" +
                "startPoint='" + startPoint + '\'' +
                ", endPoint='" + endPoint + '\'' +
                ", startLatitude=" + startLatitude +
                ", startLongitude=" + startLongitude +
                ", endLatitude=" + endLatitude +
                ", endLongitude=" + endLongitude +
                ", totalFreePlaces=" + totalFreePlaces +
                ", currentFreePlaces=" + currentFreePlaces +
                ", startTime=" + startTime +
                ", estimatedEndTime=" + estimatedEndTime +
                ", price=" + price +
                '}';
    }
}
