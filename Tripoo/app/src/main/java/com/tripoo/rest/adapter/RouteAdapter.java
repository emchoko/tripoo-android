package com.tripoo.rest.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.tripoo.R;
import com.tripoo.rest.RestRetriever;
import com.tripoo.rest.RestService;
import com.tripoo.rest.model.Booking;
import com.tripoo.rest.model.get.details.RouteDetails;
import com.tripoo.utils.Constants;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by emil on 1/9/17.
 */

public class RouteAdapter extends RecyclerView.Adapter<RouteAdapter.RouteViewHolder> {

    private static final String TAG = RouteAdapter.class.getSimpleName();

    private List<RouteDetails> routes;
    private int rowLayout;
    private Context context;
    private boolean isBooked;

    public RouteAdapter(List<RouteDetails> routes, int rowLayout, Context context, boolean b) {
        this.routes = routes;
        this.rowLayout = rowLayout;
        this.context = context;
        this.isBooked = b;
    }

    public static class RouteViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.cardStartPoint)
        TextView startPoint;
        @BindView(R.id.cardEndPoint)
        TextView endPoint;
        @BindView(R.id.cardFreePlaces)
        TextView availablePlaces;
        @BindView(R.id.cardRoutePrice)
        TextView price;
        @BindView(R.id.cardBookTripBtn)
        Button bookBtn;

        public RouteViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    @Override
    public RouteViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new RouteViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final RouteViewHolder holder, final int position) {
        holder.startPoint.setText(routes.get(position).getStartPoint());
        holder.endPoint.setText(routes.get(position).getEndPoint());
        if (isBooked) {
            holder.bookBtn.setText("UNBOOK");
        } else {
            holder.price.setText(routes.get(position).getPrice() + "");
            holder.availablePlaces.setText(routes.get(position).getCurrentFreePlaces() + "");
            holder.bookBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //TODO: POST to routes/{id}/book
                    bookRoute(Long.valueOf(routes.get(position).getId()));
                    Toast.makeText(context, "Clicked", Toast.LENGTH_SHORT).show();
                    holder.bookBtn.setText("UNBOOK");
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return routes.size();
    }


    public void bookRoute(final Long routeId){
        SharedPreferences pref = context.getSharedPreferences(Constants.userDetailsSharedPreferences, Context.MODE_PRIVATE);
        String token = pref.getString(Constants.token, "");

        RestService service = RestRetriever.getClient().create(RestService.class);

        Call<Void> call = service.bookRouteById(token, new Booking(), routeId);

        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Toast.makeText(context, response.code() + "\nBooked Route " + routeId, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Toast.makeText(context, "Failed To Book Route " + routeId, Toast.LENGTH_SHORT).show();
            }
        });
    }

}
