package com.tripoo.rest.model.get.list;

import java.util.List;

/**
 * Created by emil on 1/7/17.
 */

public class TripList {

    private Long id;
    private String date;
    private List<RouteList> routes = null;
    private Long price;
    private UserList user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<RouteList> getRoutes() {
        return routes;
    }

    public void setRoutes(List<RouteList> routes) {
        this.routes = routes;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public UserList getUser() {
        return user;
    }

    public void setUser(UserList user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "TripList{" +
                "id=" + id +
                ", date='" + date + '\'' +
                ", routes=" + routes.toString() +
                ", price=" + price +
                ", user=" + user.toString() +
                '}';
    }
}